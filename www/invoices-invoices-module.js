(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["invoices-invoices-module"],{

/***/ "./src/app/invoices/factura-express/factura-express.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/invoices/factura-express/factura-express.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\">\n      <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n        <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n      </ion-buttons>\n    <ion-title>Factura express</ion-title>    \n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <!-- info ticket -->\n  <ion-card>\n    <ion-card-header>\n      <ion-card-subtitle>Código <b> {{info.code}} </b> </ion-card-subtitle>\n      \n    </ion-card-header>\n  \n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col size=\"3\"> Subtotal  </ion-col>\n          <ion-col size=\"3\"> {{info.subtotal | currency }}  </ion-col>\n          <ion-col size=\"3\"> IVA  </ion-col>\n          <ion-col size=\"3\"> {{info.iva | currency }}  </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-col size=\"3\">   </ion-col>\n          <ion-col size=\"3\">   </ion-col>\n          <ion-col size=\"3\"> Total  </ion-col>\n          <ion-col size=\"3\"> {{info.total | currency }}  </ion-col>\n        </ion-row>\n        <a (click)=\"showDetail()\"> Ver detalle </a>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n  <!-- info ticket -->\n\n  \n  <ion-grid>\n    <ion-row>\n      <ion-col>  \n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input required matInput placeholder=\"Correo\" [(ngModel)]=\"invoiceData.email\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>  \n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input required matInput placeholder=\"Razón social\" [(ngModel)]=\"invoiceData.razon_social\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>          \n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input required matInput placeholder=\"RFC\" [(ngModel)]=\"invoiceData.RFC\" style=\"text-transform: uppercase\"  value=\"\">\n        </mat-form-field>          \n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      \n      <ion-col size=\"6\">\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n              <input matInput placeholder=\"País\" [(ngModel)]=\"invoiceData.pais\"  value=\"\">\n          </mat-form-field>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Estado\" [(ngModel)]=\"invoiceData.estado\"  value=\"\">\n        </mat-form-field>\n    </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\">\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Ciudad\" [(ngModel)]=\"invoiceData.ciudad\"  value=\"\">\n          </mat-form-field>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"Colonia\" [(ngModel)]=\"invoiceData.colonia\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"Calle\" [(ngModel)]=\"invoiceData.calle\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"CP\" [(ngModel)]=\"invoiceData.cp\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n      \n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"No. Int\" [(ngModel)]=\"invoiceData.noInt\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"No. Ext\" [(ngModel)]=\"invoiceData.noExt\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n<ion-footer class=\"footerButton\" >\n  <ion-button color=\"primary\" expand=\"block\"  style=\"background: #3399CC; color:#fff\" (click)=\"sendInvoice()\" >\n    <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Facturar\n  </ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/invoices/factura-express/factura-express.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/invoices/factura-express/factura-express.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ludm9pY2VzL2ZhY3R1cmEtZXhwcmVzcy9mYWN0dXJhLWV4cHJlc3MuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/invoices/factura-express/factura-express.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/invoices/factura-express/factura-express.component.ts ***!
  \***********************************************************************/
/*! exports provided: FacturaExpressComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FacturaExpressComponent", function() { return FacturaExpressComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ticket_detail_ticket_detail_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ticket-detail/ticket-detail.component */ "./src/app/invoices/ticket-detail/ticket-detail.component.ts");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/validator.service */ "./src/app/services/validator.service.ts");
/* harmony import */ var src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/tickets.service */ "./src/app/services/tickets.service.ts");







var FacturaExpressComponent = /** @class */ (function () {
    function FacturaExpressComponent(params, modalCtrl, generalService, validationService, ticketsService, loading) {
        this.params = params;
        this.modalCtrl = modalCtrl;
        this.generalService = generalService;
        this.validationService = validationService;
        this.ticketsService = ticketsService;
        this.loading = loading;
        this.invoiceData = {};
        this.info = this.params.get('msg');
        this.sucursal = this.params.get('sucursal');
    }
    FacturaExpressComponent.prototype.ngOnInit = function () { };
    FacturaExpressComponent.prototype.showDetail = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _ticket_detail_ticket_detail_component__WEBPACK_IMPORTED_MODULE_3__["TicketDetailComponent"],
                            componentProps: { 'msg': this.info }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    FacturaExpressComponent.prototype.backTab = function () {
        this.modalCtrl.dismiss();
    };
    FacturaExpressComponent.prototype.sendInvoice = function () {
        var _this = this;
        this.generalService.loading('Generando tu factura');
        var validation = this.validationService.validationInvoceExpress(this.invoiceData);
        if (validation.success) {
            this.ticketsService.invoiceExpress(this.invoiceData, this.info.id).subscribe(function (res) {
                _this.invoiceData = {};
                _this.generalService.msgAlert('Tu factura ha sido creada correctamente, en breve recibiras un correo ');
                setTimeout(function () {
                    _this.loading.dismiss();
                    _this.modalCtrl.dismiss({ status: 'update' });
                }, 500);
            }, function (err) {
                _this.loading.dismiss();
                _this.generalService.msgAlert('No se pudo realizar la facturación en este momento , intentalo más tarde.');
            });
        }
        else {
            this.generalService.msgAlert(validation.msg);
            return;
        }
    };
    FacturaExpressComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-factura-express',
            template: __webpack_require__(/*! ./factura-express.component.html */ "./src/app/invoices/factura-express/factura-express.component.html"),
            styles: [__webpack_require__(/*! ./factura-express.component.scss */ "./src/app/invoices/factura-express/factura-express.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__["GeneralService"], src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_5__["ValidatorService"], src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_6__["TicketsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], FacturaExpressComponent);
    return FacturaExpressComponent;
}());



/***/ }),

/***/ "./src/app/invoices/invoices.module.ts":
/*!*********************************************!*\
  !*** ./src/app/invoices/invoices.module.ts ***!
  \*********************************************/
/*! exports provided: InvoicesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesPageModule", function() { return InvoicesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _invoices_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./invoices.page */ "./src/app/invoices/invoices.page.ts");
/* harmony import */ var _factura_express_factura_express_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./factura-express/factura-express.component */ "./src/app/invoices/factura-express/factura-express.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _ticket_detail_ticket_detail_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ticket-detail/ticket-detail.component */ "./src/app/invoices/ticket-detail/ticket-detail.component.ts");










var routes = [
    {
        path: '',
        component: _invoices_page__WEBPACK_IMPORTED_MODULE_6__["InvoicesPage"]
    }, {
        path: 'factura-express',
        component: _factura_express_factura_express_component__WEBPACK_IMPORTED_MODULE_7__["FacturaExpressComponent"]
    }, {
        path: 'ticketDetail',
        component: _ticket_detail_ticket_detail_component__WEBPACK_IMPORTED_MODULE_9__["TicketDetailComponent"]
    }
    //  {
    //   path: 'modalRfc', 
    //   component: RfcComponent
    // }
];
var InvoicesPageModule = /** @class */ (function () {
    function InvoicesPageModule() {
    }
    InvoicesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatExpansionModule"]
            ],
            declarations: [_invoices_page__WEBPACK_IMPORTED_MODULE_6__["InvoicesPage"], _factura_express_factura_express_component__WEBPACK_IMPORTED_MODULE_7__["FacturaExpressComponent"], _ticket_detail_ticket_detail_component__WEBPACK_IMPORTED_MODULE_9__["TicketDetailComponent"]]
        })
    ], InvoicesPageModule);
    return InvoicesPageModule;
}());



/***/ }),

/***/ "./src/app/invoices/invoices.page.html":
/*!*********************************************!*\
  !*** ./src/app/invoices/invoices.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\">\n    <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n      <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n    </ion-buttons>\n\n    <ion-title>Factura express</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [class.show-qr-scanner]=\"isOn\">\n    <ion-grid style=\"margin-top:2em\" *ngIf=\" countSucursales > 1\">\n      <ion-row>\n          <mat-form-field style=\"width: 100% \">\n            <mat-label>Selecciona sucursal </mat-label>\n            <select matNativeControl required [(ngModel)]=\"sucursal\">\n              <option *ngFor=\"let row of rows\" value=\"{{row.id}}\" >{{row.name}}</option>\n            </select>\n          </mat-form-field>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid >\n      <ion-row>\n        <ion-col  >\n          <div class=\"textScan\" style=\"text-align: center\">\n            Para facutar tu ticket scanea el Qr del ticket \n          </div>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row >\n        <ion-col style=\"text-align: center;\" (click)=\"scann()\">\n           <ion-icon name=\"qr-scanner\" class=\"baseColor\"  style=\"font-size: 8em\" ></ion-icon>\n        </ion-col>\n        \n      </ion-row>\n  \n      <ion-row>\n        <ion-col >\n            <ion-button color=\"primary\" expand=\"block\"  (click)=\"scann()\" style=\"background: #3399CC; color:#fff\">\n              <ion-icon name=\"ios-qr-scanner-outline\"  > </ion-icon>  escanear QR\n            </ion-button>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row style=\"margin-top:1em\">\n        <ion-col >\n            <div class=\"textScan\" style=\"text-align: center\">\n              o <br> ingresa el código que aparece abajo del QR\n            </div>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col align-self-center>        \n          <ion-input placeholder=\"XXXXXXXX\" class=\"inputCode\" style=\"border: none;height: 100%; width: 100%; border: 1px solid #3399CC;\" \n            [(ngModel)]=\"inputTicket\" ></ion-input> \n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col >\n            <ion-button color=\"primary\" expand=\"block\"  (click)=\"activeSearch(inputTicket)\" style=\"background: #3399CC; color:#fff\">\n              <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Buscar\n            </ion-button>\n        </ion-col>\n      </ion-row>\n  \n  \n    </ion-grid>\n  \n  \n      \n  </ion-content>\n"

/***/ }),

/***/ "./src/app/invoices/invoices.page.scss":
/*!*********************************************!*\
  !*** ./src/app/invoices/invoices.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ludm9pY2VzL2ludm9pY2VzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/invoices/invoices.page.ts":
/*!*******************************************!*\
  !*** ./src/app/invoices/invoices.page.ts ***!
  \*******************************************/
/*! exports provided: InvoicesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesPage", function() { return InvoicesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_general_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var _services_tickets_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/tickets.service */ "./src/app/services/tickets.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_constant_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/constant.service */ "./src/app/services/constant.service.ts");
/* harmony import */ var _factura_express_factura_express_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./factura-express/factura-express.component */ "./src/app/invoices/factura-express/factura-express.component.ts");
/* harmony import */ var _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/qr-scanner/ngx */ "./node_modules/@ionic-native/qr-scanner/ngx/index.js");









var InvoicesPage = /** @class */ (function () {
    function InvoicesPage(conts, router, generalService, ticketService, loading, modalCtrl, qrScanner) {
        var _this = this;
        this.conts = conts;
        this.router = router;
        this.generalService = generalService;
        this.ticketService = ticketService;
        this.loading = loading;
        this.modalCtrl = modalCtrl;
        this.qrScanner = qrScanner;
        this.isOn = false;
        this.rows = [];
        this.countSucursales = 0;
        this.company = this.conts.companyApp;
        this.inputTicket = 'Bn30ZP';
        setTimeout(function () {
            _this.getOffices();
        }, 300);
    }
    InvoicesPage.prototype.ngOnInit = function () {
    };
    InvoicesPage.prototype.backTab = function () {
        this.router.navigate(['']);
    };
    InvoicesPage.prototype.activeSearch = function (code) {
        var _this = this;
        if (!this.sucursal) {
            this.generalService.msgAlert('Debes seleccionar sucursal');
            return;
        }
        this.codeInput = code;
        this.generalService.loading('Buscando ticket');
        var params = {
            "companyId": this.company,
            "sucursal": this.sucursal,
            "noTicket": code
        };
        this.ticketService.searchTicketExpress(params, this.user_token).subscribe(function (res) {
            _this.loading.dismiss();
            if (res.code == 501) {
                _this.generalService.msgAlert(res.msg);
            }
            else if (res.code == 200) {
                setTimeout(function () {
                    _this.openModal(res.msg);
                }, 400);
            }
        });
    };
    InvoicesPage.prototype.openModal = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _factura_express_factura_express_component__WEBPACK_IMPORTED_MODULE_7__["FacturaExpressComponent"],
                            componentProps: { 'msg': params, sucursal: this.sucursal }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (res) {
                            console.log(':: cerro modal', res);
                            if (res.data.status == 'invoice') {
                                // Pregunta si quiere facturar //
                                // this.sendInvoice(this.inputTicket);
                            }
                            else if (res.data.status == 'update') {
                                _this.backTab();
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    InvoicesPage.prototype.getOffices = function () {
        var _this = this;
        this.ticketService.sucursalesCompany(this.conts.companyApp).subscribe(function (res) {
            _this.countSucursales = res.msg.length;
            _this.rows = res.msg;
            if (_this.countSucursales == 1) {
                _this.sucursal = res.msg[0].id;
            }
        }, function (err) {
            console.log(' hay errororororororro');
        });
    };
    InvoicesPage.prototype.scann = function () {
        var _this = this;
        // Optionally request the permission early
        this.qrScanner.prepare().then(function (status) {
            if (status.authorized) {
                _this.qrScanner.show();
                document.getElementsByTagName("body")[0].style.opacity = "0";
                _this.qrScan = _this.qrScanner.scan().subscribe(function (textFound) {
                    document.getElementsByTagName("body")[0].style.opacity = "1";
                    _this.qrScan.unsubscribe();
                    // console.info(textFound);
                    // console.log('::: debio scanear :::');
                    _this.activeSearch(textFound);
                    setTimeout(function () {
                    }, 20);
                }, function (err) {
                    console.log(JSON.stringify(err));
                });
            }
            else if (status.denied) {
            }
        });
    };
    InvoicesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-invoices',
            template: __webpack_require__(/*! ./invoices.page.html */ "./src/app/invoices/invoices.page.html"),
            styles: [__webpack_require__(/*! ./invoices.page.scss */ "./src/app/invoices/invoices.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_constant_service__WEBPACK_IMPORTED_MODULE_6__["ConstantService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_general_service__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], _services_tickets_service__WEBPACK_IMPORTED_MODULE_4__["TicketsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"], _ionic_native_qr_scanner_ngx__WEBPACK_IMPORTED_MODULE_8__["QRScanner"]])
    ], InvoicesPage);
    return InvoicesPage;
}());



/***/ }),

/***/ "./src/app/invoices/ticket-detail/ticket-detail.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/invoices/ticket-detail/ticket-detail.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"baseApp\">\n        <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n          <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n        </ion-buttons>\n      <ion-title>Detalle ticket</ion-title>    \n  \n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content> \n    <!-- info ticket -->\n    <ion-list>\n      <ion-item *ngFor=\"let row of rows\">\n          <ion-card style=\"width:100%\">\n            <ion-card-header>\n              <ion-card-subtitle> <b> {{row.descripcion}} </b> </ion-card-subtitle>\n              \n            </ion-card-header>\n          \n            <ion-card-content>\n              <ion-grid>\n                <ion-row>\n                  <ion-col size=\"6\"> Valor unitario  </ion-col>\n                  <ion-col size=\"6\" style=\"text-align: right\"> {{row.valor_unitario | currency }}  </ion-col>\n                </ion-row>\n\n                <ion-row>\n                  <ion-col size=\"6\"> Cantidad {{row.cantidad}}</ion-col>                  \n                  <ion-col size=\"6\" style=\"text-align: right\"> {{row.cantidad | currency }}  </ion-col>\n                  \n                </ion-row>\n                \n                <ion-row>\n                  <ion-col size=\"6\"> Importe </ion-col>\n                  <ion-col size=\"6\" style=\"text-align: right\"> {{row.importe | currency }}  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-card-content>\n          </ion-card>\n      </ion-item>\n    </ion-list>\n  \n    <!-- info ticket -->\n    \n  </ion-content>"

/***/ }),

/***/ "./src/app/invoices/ticket-detail/ticket-detail.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/invoices/ticket-detail/ticket-detail.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ludm9pY2VzL3RpY2tldC1kZXRhaWwvdGlja2V0LWRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/invoices/ticket-detail/ticket-detail.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/invoices/ticket-detail/ticket-detail.component.ts ***!
  \*******************************************************************/
/*! exports provided: TicketDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketDetailComponent", function() { return TicketDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var TicketDetailComponent = /** @class */ (function () {
    function TicketDetailComponent(params, modalCtrl) {
        this.params = params;
        this.modalCtrl = modalCtrl;
        this.rows = [];
        this.info = this.params.get('msg');
        console.log(':::--:::', this.info);
        this.rows = this.info.conceptos;
    }
    TicketDetailComponent.prototype.ngOnInit = function () { };
    TicketDetailComponent.prototype.backTab = function () {
        this.modalCtrl.dismiss();
    };
    TicketDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ticket-detail',
            template: __webpack_require__(/*! ./ticket-detail.component.html */ "./src/app/invoices/ticket-detail/ticket-detail.component.html"),
            styles: [__webpack_require__(/*! ./ticket-detail.component.scss */ "./src/app/invoices/ticket-detail/ticket-detail.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], TicketDetailComponent);
    return TicketDetailComponent;
}());



/***/ })

}]);
//# sourceMappingURL=invoices-invoices-module.js.map