(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["session-login-login-module"],{

/***/ "./src/app/session/login/login.module.ts":
/*!***********************************************!*\
  !*** ./src/app/session/login/login.module.ts ***!
  \***********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/session/login/login.page.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");








var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatExpansionModule"]
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/session/login/login.page.html":
/*!***********************************************!*\
  !*** ./src/app/session/login/login.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>login</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form class=\"example-form\">\n    <mat-form-field class=\"example-full-width\">\n      <input matInput placeholder=\"Favorite food\" value=\"Sushi\">\n    </mat-form-field>\n  \n  </form>\n</ion-content> -->\n<ion-content style=\"background-image: url('assets/imgs/system/Inicio-2732x4857.jpg');\" text-center>\n\n  <div class=\"conta\">\n    <img class=\"logo\" src=\"/assets/imgs/system/icon-clickGasolineras.png\">\n\n    <div class=\"title\">Iniciar sesión</div>\n\n\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Usuario\" [(ngModel)]=\"user.username\">\n          </mat-form-field>\n       </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n              <input matInput placeholder=\"Contraseña\" [(ngModel)]=\"user.password\" type=\"password\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row justify-content-end>\n        <ion-col >\n          <div class=\"pass\" text-right>\n            <span [routerLink]=\"['/session/forgot-password']\">Olvidé mi contraseña</span>\n          </div>\n        </ion-col>\n      </ion-row>\n      \n\n      <ion-row>\n        <ion-col>\n            <!-- <ion-button expand=\"block\" (click)=\"authSession()\" >Iniciar sesión</ion-button> -->\n            <ion-button expand=\"block\" (click)=\"authSession()\" >Iniciar sesión</ion-button>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n    \n\n    \n\n  </div>\n\n</ion-content>\n\n\n<ion-footer class=\"footerButton\" [routerLink]=\"['/session/register']\">\n  ¿Aún no tienes cuenta? Registrate aquí\n</ion-footer>"

/***/ }),

/***/ "./src/app/session/login/login.page.scss":
/*!***********************************************!*\
  !*** ./src/app/session/login/login.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".logo {\n  margin-top: 1em; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3Nlc3Npb24vbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2Vzc2lvbi9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9nb3tcbiAgICBtYXJnaW4tdG9wOiAxZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/session/login/login.page.ts":
/*!*********************************************!*\
  !*** ./src/app/session/login/login.page.ts ***!
  \*********************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _register_register_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../register/register.page */ "./src/app/session/register/register.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/validator.service */ "./src/app/services/validator.service.ts");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");








var LoginPage = /** @class */ (function () {
    function LoginPage(modalController, router, toast, validator, userService, generalServices) {
        this.modalController = modalController;
        this.router = router;
        this.toast = toast;
        this.validator = validator;
        this.userService = userService;
        this.generalServices = generalServices;
        this.user = {};
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.openRegister = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _register_register_page__WEBPACK_IMPORTED_MODULE_3__["RegisterPage"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoginPage.prototype.authSession = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var validation, toast;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        validation = this.validator.validationLogin(this.user);
                        localStorage.clear();
                        if (!validation.success) return [3 /*break*/, 1];
                        this.userService.authUser(this.user).subscribe(function (res) {
                            var session = localStorage.setItem('concentGasApp', encodeURIComponent(JSON.stringify(res.msg)));
                            setTimeout(function () {
                                _this.generalServices.listenerSession.emit(true);
                                _this.router.navigate(['sucursales']);
                            }, 500);
                        }, function (error) {
                            var msg = error.error.msg;
                            _this.generalServices.msgAlert(msg);
                        });
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.toast.create({
                            message: validation.msg,
                            duration: 2000,
                            position: 'top'
                        })];
                    case 2:
                        toast = _a.sent();
                        toast.present();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/session/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/session/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_5__["ValidatorService"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"],
            src_app_services_general_service__WEBPACK_IMPORTED_MODULE_7__["GeneralService"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=session-login-login-module.js.map