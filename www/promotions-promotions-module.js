(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["promotions-promotions-module"],{

/***/ "./src/app/promotions/promotions.module.ts":
/*!*************************************************!*\
  !*** ./src/app/promotions/promotions.module.ts ***!
  \*************************************************/
/*! exports provided: PromotionsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionsPageModule", function() { return PromotionsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _promotions_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./promotions.page */ "./src/app/promotions/promotions.page.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");








var routes = [
    {
        path: '',
        component: _promotions_page__WEBPACK_IMPORTED_MODULE_6__["PromotionsPage"]
    }
];
var PromotionsPageModule = /** @class */ (function () {
    function PromotionsPageModule() {
    }
    PromotionsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatExpansionModule"]
            ],
            declarations: [_promotions_page__WEBPACK_IMPORTED_MODULE_6__["PromotionsPage"]]
        })
    ], PromotionsPageModule);
    return PromotionsPageModule;
}());



/***/ }),

/***/ "./src/app/promotions/promotions.page.html":
/*!*************************************************!*\
  !*** ./src/app/promotions/promotions.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"baseApp\">\n        <ion-buttons slot=\"start\">\n          <ion-menu-button></ion-menu-button>\n        </ion-buttons>\n      <ion-title  >Promociones </ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n<ion-content>\n  \n  <ion-grid *ngIf=\"promotions.length == 0\"> \n    <ion-row>\n      <ion-col style=\"text-align: center\">\n          <i class=\"material-icons\" style=\"color: #3399CC;font-size: 5em;\">card_giftcard</i>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col style=\"text-align: center\">\n          No hay promociones disponibles\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n\n  <ion-card *ngFor=\"let row of promotions\">\n    <img src=\"{{row.imgs_promo[0].image}}\" style=\"width: 50%;margin: auto;\">\n      <ion-card-header>\n        <ion-card-title>{{row.name}}</ion-card-title>\n      </ion-card-header>\n    \n      <ion-card-content>\n          {{row.description}}\n\n          <small style=\"color:#ccc\"> Promoción válida de  {{row.dateIni | date: 'dd/MM/yyyy' }} a {{ row.dateEnd | date: 'dd/MM/yyyy'  }} </small>\n      </ion-card-content>\n    </ion-card>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/promotions/promotions.page.scss":
/*!*************************************************!*\
  !*** ./src/app/promotions/promotions.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb21vdGlvbnMvcHJvbW90aW9ucy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/promotions/promotions.page.ts":
/*!***********************************************!*\
  !*** ./src/app/promotions/promotions.page.ts ***!
  \***********************************************/
/*! exports provided: PromotionsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromotionsPage", function() { return PromotionsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_tickets_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/tickets.service */ "./src/app/services/tickets.service.ts");



var PromotionsPage = /** @class */ (function () {
    function PromotionsPage(ticketService) {
        var _this = this;
        this.ticketService = ticketService;
        this.promotions = [];
        var b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.userId = b['id'];
        this.userToken = b['session_token'];
        this.company = b.company;
        console.log(this.company);
        setTimeout(function () {
            _this.getPromos();
        }, 300);
    }
    PromotionsPage.prototype.ngOnInit = function () {
    };
    PromotionsPage.prototype.getPromos = function () {
        var _this = this;
        this.ticketService.promociones(this.userId, this.company.id, this.userToken).subscribe(function (res) {
            console.log(res.msg);
            _this.promotions.push(res.msg);
            console.log(_this.promotions[0].imgs_promo[0].image);
        });
    };
    PromotionsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-promotions',
            template: __webpack_require__(/*! ./promotions.page.html */ "./src/app/promotions/promotions.page.html"),
            styles: [__webpack_require__(/*! ./promotions.page.scss */ "./src/app/promotions/promotions.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_tickets_service__WEBPACK_IMPORTED_MODULE_2__["TicketsService"]])
    ], PromotionsPage);
    return PromotionsPage;
}());



/***/ })

}]);
//# sourceMappingURL=promotions-promotions-module.js.map