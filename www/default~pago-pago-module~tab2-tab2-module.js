(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~pago-pago-module~tab2-tab2-module"],{

/***/ "./src/app/pago/ticket/ticket.component.html":
/*!***************************************************!*\
  !*** ./src/app/pago/ticket/ticket.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\" >\n    <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n      <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n    </ion-buttons>\n    <ion-title>Ticket </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n          <ion-select placeholder=\"Seleccione tarjeta\" class=\"selectCustom\" [(ngModel)]=\"cardPay\" *ngIf=\"typeView == 'pay' \" >\n            <ion-select-option *ngFor=\"let row of cards\" value=\"{{row.id}}\"  >XXXX-{{row.number}}</ion-select-option>\n            <ion-select-option value=\"new\">Agregar tarjeta</ion-select-option>\n          </ion-select>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"head-table\"> \n      <ion-col size=\"4\" class=\"divider\"> <b> Catidad</b> </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> <b>Producto</b> </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> <b>Importe</b>  </ion-col>\n    </ion-row>\n\n    <ion-row *ngFor=\"let row of conceptos\"> \n      <ion-col size=\"4\" class=\"divider\"> {{ row.cantidad }} </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> {{row.descripcion}} </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> {{ row.importe | currency }}  </ion-col>\n    </ion-row>\n\n  <!--  Datos de ticket final -->\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>Subtotal</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\">  {{ subtotal | currency }} </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>I.V.A.</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\"> {{ iva | currency }}</ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>Total</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\"> {{ total | currency }} </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-footer class=\"footerButton\" *ngIf=\"typeView == 'pay' \">\n  <ion-button color=\"primary\" expand=\"block\"  (click)=\"PayTicket()\" style=\"background: #3399CC; color:#fff\">\n    <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Pagar\n  </ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pago/ticket/ticket.component.scss":
/*!***************************************************!*\
  !*** ./src/app/pago/ticket/ticket.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".head-table {\n  background-color: #3399CC;\n  color: #fff; }\n\n.divider {\n  border-left: 1px solid #7f7f7f;\n  border-right: 1px solid #7f7f7f;\n  border-bottom: 1px solid #7f7f7f; }\n\n.text-fin-ticket {\n  font-size: 12px; }\n\n.selectCustom {\n  border: 1px solid #3399CC; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3BhZ28vdGlja2V0L3RpY2tldC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUF5QjtFQUN6QixXQUNKLEVBQUE7O0FBRUE7RUFDSSw4QkFBOEI7RUFDOUIsK0JBQStCO0VBQy9CLGdDQUFnQyxFQUFBOztBQUdwQztFQUNJLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSx5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ28vdGlja2V0L3RpY2tldC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkLXRhYmxle1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMzk5Q0M7XG4gICAgY29sb3I6ICNmZmZcbn1cblxuLmRpdmlkZXJ7XG4gICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjN2Y3ZjdmO1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICM3ZjdmN2Y7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM3ZjdmN2Y7XG59XG5cbi50ZXh0LWZpbi10aWNrZXR7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uc2VsZWN0Q3VzdG9te1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzk5Q0M7XG59Il19 */"

/***/ }),

/***/ "./src/app/pago/ticket/ticket.component.ts":
/*!*************************************************!*\
  !*** ./src/app/pago/ticket/ticket.component.ts ***!
  \*************************************************/
/*! exports provided: TicketComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketComponent", function() { return TicketComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/tickets.service */ "./src/app/services/tickets.service.ts");





var TicketComponent = /** @class */ (function () {
    function TicketComponent(params, userServices, ticketService, modalCtrl) {
        var _this = this;
        this.params = params;
        this.userServices = userServices;
        this.ticketService = ticketService;
        this.modalCtrl = modalCtrl;
        this.conceptos = [];
        this.cards = [];
        var b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.userId = b['id'];
        this.userToken = b['session_token'];
        this.company = b.company;
        this.user_token = b.session_token;
        var data = this.params.get('msg');
        var type = this.params.get('type');
        this.conceptos = data.conceptos;
        this.subtotal = data.subtotal;
        this.total = data.total;
        this.iva = data.iva;
        this.ticketId = data.id;
        if (type != 'pay') {
            setTimeout(function () {
                _this.typeView = type;
            }, 300);
        }
        //console.log('::: data', data);
        setTimeout(function () {
            _this.getList();
        }, 200);
    }
    TicketComponent.prototype.ngOnInit = function () { };
    TicketComponent.prototype.PayTicket = function () {
        this.ticketService.payTicket({ cardId: this.cardPay }, this.userId, this.ticketId, this.userToken).subscribe(function (res) {
            console.log(res);
        });
    };
    TicketComponent.prototype.getList = function () {
        var _this = this;
        this.userServices.listCards(this.userId, this.userToken).subscribe(function (res) {
            _this.cards = res.msg;
        });
    };
    TicketComponent.prototype.backTab = function () {
        this.modalCtrl.dismiss({});
    };
    TicketComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ticket',
            template: __webpack_require__(/*! ./ticket.component.html */ "./src/app/pago/ticket/ticket.component.html"),
            styles: [__webpack_require__(/*! ./ticket.component.scss */ "./src/app/pago/ticket/ticket.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_4__["TicketsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], TicketComponent);
    return TicketComponent;
}());



/***/ }),

/***/ "./src/app/services/constant.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/constant.service.ts ***!
  \**********************************************/
/*! exports provided: ConstantService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConstantService", function() { return ConstantService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConstantService = /** @class */ (function () {
    function ConstantService() {
        this.isprodCnk = false;
        this.baseUrl = window.location.protocol + '//' + window.location.host;
        this.host = window.location.host;
        this.apiConnect = 'http://192.168.8.136/proveedores-api/web/app_dev.php/api/v1/';
        this.conektaKey = (this.isprodCnk) ? "--" : "key_Nm78wRyBzzUy6Zssjf2guZg";
        this.companyApp = 'c1e0ea8a-87d4-11e9-9e4b-659de6a89800';
    }
    ConstantService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConstantService);
    return ConstantService;
}());



/***/ }),

/***/ "./src/app/services/tickets.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/tickets.service.ts ***!
  \*********************************************/
/*! exports provided: TicketsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketsService", function() { return TicketsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _constant_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constant.service */ "./src/app/services/constant.service.ts");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");





var TicketsService = /** @class */ (function () {
    function TicketsService(http, constant, httpNative) {
        this.http = http;
        this.constant = constant;
        this.httpNative = httpNative;
        this.headers = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
        this.api_url = this.constant.apiConnect;
    }
    TicketsService.prototype.createHeader = function (token) {
        this.headers = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded', 'token': token }) };
    };
    TicketsService.prototype.searchTicket = function (params, token) {
        this.createHeader(token);
        var url = this.api_url + 'tickets/buscar-code';
        return this.http.post(url, params, this.headers);
    };
    TicketsService.prototype.payTicket = function (params, userId, ticketId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/ticket/' + ticketId + '/pay';
        return this.http.post(url, params, this.headers);
    };
    TicketsService.prototype.paymentsTickets = function (userId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/tickets/payment';
        return this.http.get(url, this.headers);
    };
    TicketsService.prototype.getTicket = function (userId, ticketId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/ticket/' + ticketId;
        return this.http.get(url, this.headers);
    };
    TicketsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _constant_service__WEBPACK_IMPORTED_MODULE_3__["ConstantService"], _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__["HTTP"]])
    ], TicketsService);
    return TicketsService;
}());



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _constant_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constant.service */ "./src/app/services/constant.service.ts");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");





var UserService = /** @class */ (function () {
    function UserService(http, constant, httpNative) {
        this.http = http;
        this.constant = constant;
        this.httpNative = httpNative;
        this.headers = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
        this.api_url = this.constant.apiConnect;
    }
    UserService.prototype.createHeader = function (token) {
        this.headers = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded', 'token': token }) };
    };
    UserService.prototype.authUser = function (params) {
        console.log("::  parametros para enviasr ::", params);
        return this.http.post(this.api_url + 'user/login', params, this.headers);
    };
    UserService.prototype.restorePassword = function (email) {
        var url = this.api_url + 'user/restore_password';
        return this.http.post(url, { 'email': email }, this.headers);
    };
    UserService.prototype.registerUser = function (params) {
        this.headers = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
        var url = this.api_url + 'user/register';
        return this.http.post(url, params, this.headers);
    };
    //user/{userId}/change-password
    UserService.prototype.changePassword = function (userId, params, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/change-password';
        return this.http.post(url, params, this.headers);
    };
    UserService.prototype.userInfo = function (userId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId;
        return this.http.get(url, this.headers);
    };
    UserService.prototype.addCard = function (userId, params, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/addCard';
        return this.http.post(url, params, this.headers);
    };
    UserService.prototype.listCards = function (userId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/cards';
        return this.http.get(url, this.headers);
    };
    UserService.prototype.addRfc = function (params, userId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/addRfc';
        return this.http.post(url, params, this.headers);
    };
    UserService.prototype.listRfcs = function (userId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/Rfcs';
        return this.http.get(url, this.headers);
    };
    UserService.prototype.payments = function (userId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/tickets/payment';
        return this.http.get(url, this.headers);
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _constant_service__WEBPACK_IMPORTED_MODULE_3__["ConstantService"], _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__["HTTP"]])
    ], UserService);
    return UserService;
}());



/***/ })

}]);
//# sourceMappingURL=default~pago-pago-module~tab2-tab2-module.js.map