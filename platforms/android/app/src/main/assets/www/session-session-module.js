(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["session-session-module"],{

/***/ "./src/app/session/forgot-password/forgot-password.page.html":
/*!*******************************************************************!*\
  !*** ./src/app/session/forgot-password/forgot-password.page.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-icon name=\"arrow-round-back\"  style=\"color: black; font-size: 2em \" [routerLink]=\"['/session/login']\" ></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content style=\"background-image: url('assets/imgs/system/Inicio-2732x4857.jpg');\" text-center>\n\n  <div class=\"conta\" style=\"margin-top: 15%;\">\n    <img class=\"logo\" src=\"assets/imgs/system/icon-clickGasolineras.png\">\n\n    <div class=\"title\">Recuperar contraseña</div>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Usuario\" [(ngModel)]=\"email\">\n          </mat-form-field>\n       </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n            <ion-button expand=\"block\" (click)=\"restorePassword()\" >Recuperar contraseña</ion-button>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n    \n\n    \n\n  </div>\n\n</ion-content>\n\n\n"

/***/ }),

/***/ "./src/app/session/forgot-password/forgot-password.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/session/forgot-password/forgot-password.page.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-md:after {\n  left: 0;\n  bottom: -5px;\n  background-position: left 0 top -2px;\n  position: absolute;\n  width: 100%;\n  height: 5px;\n  background-repeat: repeat-x;\n  content: \"\";\n  background-image: none; }\n\n.header-md:after {\n  left: 0;\n  bottom: -5px;\n  background-position: left 0 top -2px;\n  position: absolute;\n  width: 100%;\n  height: 5px;\n  background-image: none;\n  background-repeat: repeat-x;\n  content: \"\"; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3Nlc3Npb24vZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxPQUFPO0VBQ1AsWUFBWTtFQUNaLG9DQUFvQztFQUNwQyxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWCwyQkFBMkI7RUFDM0IsV0FBVztFQUNYLHNCQUNKLEVBQUE7O0FBR0E7RUFDSSxPQUFPO0VBQ1AsWUFBWTtFQUNaLG9DQUFvQztFQUNwQyxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFdBQVc7RUFDWCxzQkFBcUI7RUFDckIsMkJBQTJCO0VBQzNCLFdBQVcsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Nlc3Npb24vZm9yZ290LXBhc3N3b3JkL2ZvcmdvdC1wYXNzd29yZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyLW1kOmFmdGVyIHtcbiAgICBsZWZ0OiAwO1xuICAgIGJvdHRvbTogLTVweDtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0IDAgdG9wIC0ycHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNXB4O1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6bm9uZVxufVxuXG5cbi5oZWFkZXItbWQ6YWZ0ZXIge1xuICAgIGxlZnQ6IDA7XG4gICAgYm90dG9tOiAtNXB4O1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgMCB0b3AgLTJweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA1cHg7XG4gICAgYmFja2dyb3VuZC1pbWFnZTpub25lO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcbiAgICBjb250ZW50OiBcIlwiO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/session/forgot-password/forgot-password.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/session/forgot-password/forgot-password.page.ts ***!
  \*****************************************************************/
/*! exports provided: ForgotPasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotPasswordPage", function() { return ForgotPasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");





var ForgotPasswordPage = /** @class */ (function () {
    function ForgotPasswordPage(router, generalService, userService) {
        this.router = router;
        this.generalService = generalService;
        this.userService = userService;
        this.email = null;
    }
    ForgotPasswordPage.prototype.ngOnInit = function () {
    };
    ForgotPasswordPage.prototype.restorePassword = function () {
        var _this = this;
        if (this.email == '' || this.email == undefined) {
            this.generalService.msgAlert('Debes ingresar tu correo ');
            return;
        }
        this.userService.restorePassword(this.email).subscribe(function (res) {
            console.log(':: enviar mail de restauració  n ::', res);
        });
        this.userService.restorePassword(this.email).subscribe(function (res) {
            if (res.success) {
                _this.generalService.msgAlert('Tu petición se ha recibido, en breve recibiras un correo con tu nuevo acceso');
                _this.email = null;
            }
            if (res.code == 401) {
                _this.generalService.msgAlert(res.msg);
            }
        }, function (error) {
            _this.generalService.msgAlert(error.error.msg);
        });
    };
    ForgotPasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-forgot-password',
            template: __webpack_require__(/*! ./forgot-password.page.html */ "./src/app/session/forgot-password/forgot-password.page.html"),
            styles: [__webpack_require__(/*! ./forgot-password.page.scss */ "./src/app/session/forgot-password/forgot-password.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], src_app_services_general_service__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]])
    ], ForgotPasswordPage);
    return ForgotPasswordPage;
}());



/***/ }),

/***/ "./src/app/session/home/home.component.html":
/*!**************************************************!*\
  !*** ./src/app/session/home/home.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>login</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form class=\"example-form\">\n    <mat-form-field class=\"example-full-width\">\n      <input matInput placeholder=\"Favorite food\" value=\"Sushi\">\n    </mat-form-field>\n  \n  </form>\n</ion-content> -->\n<ion-content style=\"background-image: url('assets/imgs/system/Inicio-2732x4857.jpg');\" text-center>\n\n  <div class=\"conta\" style=\"margin-top: 30%;\">\n    <h3 class=\"title\">Gas App </h3>\n    <img class=\"logo\" src=\"/assets/imgs/system/icon-clickGasolineras.png\">\n\n\n    <ion-grid>\n      <ion-row style=\"margin-top: 1em;\">\n        <ion-col style=\"text-align:center\">\n          Inicia sesión y ahorra tiempo facturando tu gasolina <br> o<br> factura de forma exprés\n        </ion-col>\n      </ion-row>\n      <ion-row style=\"margin-top: 2em;\">\n        <ion-col>\n            <!-- <ion-button expand=\"block\" (click)=\"authSession()\" >Iniciar sesión</ion-button> -->\n            <ion-segment (ionChange)=\"segmentChanged($event)\" [(ngModel)]=\"section\"  >\n                <ion-segment-button value=\"login\">\n                  <ion-label>Iniciar sesión </ion-label>\n                </ion-segment-button>\n                <ion-segment-button value=\"facturar\">\n                  <ion-label>Factura express</ion-label>\n                </ion-segment-button>\n              </ion-segment>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n    \n\n    \n\n  </div>\n\n</ion-content>\n\n"

/***/ }),

/***/ "./src/app/session/home/home.component.scss":
/*!**************************************************!*\
  !*** ./src/app/session/home/home.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-segment {\n  border: 1px solid #3399CC;\n  border-radius: 20px; }\n\nion-segment-button {\n  border-radius: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3Nlc3Npb24vaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQXlCO0VBQ3pCLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLG1CQUFtQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2Vzc2lvbi9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2VnbWVudHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzM5OUNDO1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG59XG5cbmlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/session/home/home.component.ts":
/*!************************************************!*\
  !*** ./src/app/session/home/home.component.ts ***!
  \************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var HomeComponent = /** @class */ (function () {
    function HomeComponent(router, nav) {
        this.router = router;
        this.nav = nav;
        this.user = {};
        this.section = 'lost';
    }
    HomeComponent.prototype.ngOnInit = function () { };
    HomeComponent.prototype.segmentChanged = function (ev) {
        var _this = this;
        if (ev.detail.value == 'login') {
            this.nav.navigateForward('session/login');
            setTimeout(function () {
                _this.section = 'lost';
            }, 500);
            //this.router.navigate(['session/login']);  
        }
        else if (ev.detail.value == 'facturar') {
            setTimeout(function () {
                _this.section = 'lost';
            }, 500);
            this.router.navigate(['invoice']);
        }
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/session/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/session/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/session/login/login.page.html":
/*!***********************************************!*\
  !*** ./src/app/session/login/login.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-header>\n    <ion-toolbar color=\"baseApp\" >\n      <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n        <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n      </ion-buttons>\n      <ion-title>Iniciar sesión </ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n<ion-content style=\"background-image: url('assets/imgs/system/Inicio-2732x4857.jpg');\" text-center>\n\n  <div class=\"conta\" style=\"margin-top: 15%;\">\n    <img class=\"logo\" src=\"/assets/imgs/system/icon-clickGasolineras.png\">\n\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Usuario\" [(ngModel)]=\"user.username\">\n          </mat-form-field>\n       </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n              <input matInput placeholder=\"Contraseña\" [(ngModel)]=\"user.password\" type=\"password\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row justify-content-end>\n        <ion-col >\n          <div class=\"pass\" text-right>\n            <span [routerLink]=\"['/session/forgot-password']\">Olvidé mi contraseña</span>\n          </div>\n        </ion-col>\n      </ion-row>\n      \n\n      <ion-row>\n        <ion-col>\n            <!-- <ion-button expand=\"block\" (click)=\"authSession()\" >Iniciar sesión</ion-button> -->\n            <ion-button expand=\"block\" (click)=\"authSession()\" style=\"background-color: #3399CC\">Iniciar sesión</ion-button>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n    \n\n    \n\n  </div>\n\n</ion-content>\n\n\n<ion-footer class=\"footerButton\" [routerLink]=\"['/session/register']\">\n  ¿Aún no tienes cuenta? Registrate aquí\n</ion-footer>"

/***/ }),

/***/ "./src/app/session/login/login.page.scss":
/*!***********************************************!*\
  !*** ./src/app/session/login/login.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".logo {\n  margin-top: 1em; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3Nlc3Npb24vbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2Vzc2lvbi9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9nb3tcbiAgICBtYXJnaW4tdG9wOiAxZW07XG59Il19 */"

/***/ }),

/***/ "./src/app/session/login/login.page.ts":
/*!*********************************************!*\
  !*** ./src/app/session/login/login.page.ts ***!
  \*********************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _register_register_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../register/register.page */ "./src/app/session/register/register.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/validator.service */ "./src/app/services/validator.service.ts");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");








var LoginPage = /** @class */ (function () {
    function LoginPage(modalController, router, toast, validator, userService, generalServices, navCtrl) {
        this.modalController = modalController;
        this.router = router;
        this.toast = toast;
        this.validator = validator;
        this.userService = userService;
        this.generalServices = generalServices;
        this.navCtrl = navCtrl;
        this.user = {};
        //this.user.username = 'ing.omarkaballero+3@gmail.com';
        this.user.username = 'ing.omarkaballero+3@gmail.com';
        this.user.password = "ee0bc188";
        //this.user.password = "ee0bc188";
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.openRegister = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _register_register_page__WEBPACK_IMPORTED_MODULE_3__["RegisterPage"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LoginPage.prototype.authSession = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var validation, toast;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        validation = this.validator.validationLogin(this.user);
                        localStorage.clear();
                        if (!validation.success) return [3 /*break*/, 1];
                        this.userService.authUser(this.user).subscribe(function (res) {
                            var session = localStorage.setItem('concentGasApp', encodeURIComponent(JSON.stringify(res.msg)));
                            setTimeout(function () {
                                _this.generalServices.listenerSession.emit(true);
                                _this.router.navigate(['pago']);
                            }, 500);
                        }, function (error) {
                            var msg = error.error.msg;
                            _this.generalServices.msgAlert(msg);
                        });
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.toast.create({
                            message: validation.msg,
                            duration: 2000,
                            position: 'top'
                        })];
                    case 2:
                        toast = _a.sent();
                        toast.present();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.backTab = function () {
        this.navCtrl.pop();
        //this.router.navigate(['/']);  
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/session/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/session/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_5__["ValidatorService"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"],
            src_app_services_general_service__WEBPACK_IMPORTED_MODULE_7__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ }),

/***/ "./src/app/session/register/register.page.html":
/*!*****************************************************!*\
  !*** ./src/app/session/register/register.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\" >\n    <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n      <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" [routerLink]=\"['/session/login']\" ></ion-icon>\n    </ion-buttons>\n    \n    <ion-title>Registrar</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content style=\"background-image: url('assets/imgs/system/Inicio-2732x4857.jpg');\" text-center>\n\n  <div class=\"conta\" style=\"margin-top: 15%;\">\n    <img class=\"logo\" src=\"/assets/imgs/system/icon-clickGasolineras.png\">\n\n    <div class=\"title\">Registro</div>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Nombre\" [(ngModel)]=\"register.name\">\n          </mat-form-field>\n       </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Apellidos\" [(ngModel)]=\"register.last_name\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Correo\" [(ngModel)]=\"register.email\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n              <input matInput placeholder=\"Contraseña\" [(ngModel)]=\"register.password\" type=\"password\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n              <input matInput placeholder=\"Confirmar contraseña\" [(ngModel)]=\"register.cpassword\" type=\"password\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n            <ion-button expand=\"block\" (click)=\"registerUser()\" >Registrar</ion-button>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n  </div>\n\n</ion-content>\n\n\n<ion-footer class=\"footerButton\" [routerLink]=\"['/session/login']\">\n  ¿Ya tienes cuenta? Inicia sesión\n</ion-footer>"

/***/ }),

/***/ "./src/app/session/register/register.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/session/register/register.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Nlc3Npb24vcmVnaXN0ZXIvcmVnaXN0ZXIucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/session/register/register.page.ts":
/*!***************************************************!*\
  !*** ./src/app/session/register/register.page.ts ***!
  \***************************************************/
/*! exports provided: RegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/validator.service */ "./src/app/services/validator.service.ts");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var src_app_services_constant_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/constant.service */ "./src/app/services/constant.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var RegisterPage = /** @class */ (function () {
    function RegisterPage(router, validationService, userService, generalService, constant) {
        this.router = router;
        this.validationService = validationService;
        this.userService = userService;
        this.generalService = generalService;
        this.constant = constant;
        this.register = {};
        console.log(this.constant.companyApp);
        this.register.company = this.constant.companyApp;
    }
    RegisterPage.prototype.ngOnInit = function () {
    };
    RegisterPage.prototype.registerUser = function () {
        var _this = this;
        var validation = this.validationService.validationRegister(this.register);
        if (validation.success) {
            this.userService.registerUser(this.register).subscribe(function (res) {
                if (res.code != 200) {
                    _this.generalService.msgAlert(res.msg);
                    return;
                }
                var msg = 'Usuario creado correctamente';
                _this.generalService.msgAlert(msg);
                _this.register = {};
                setTimeout(function () {
                    var session = localStorage.setItem('concentGasApp', encodeURIComponent(JSON.stringify(res.msg)));
                    setTimeout(function () {
                        _this.router.navigate(['tabs']);
                    }, 500);
                }, 2000);
            });
        }
        else {
            this.generalService.msgAlert(validation.msg);
            return;
        }
    };
    RegisterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.page.html */ "./src/app/session/register/register.page.html"),
            styles: [__webpack_require__(/*! ./register.page.scss */ "./src/app/session/register/register.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_2__["ValidatorService"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__["GeneralService"], src_app_services_constant_service__WEBPACK_IMPORTED_MODULE_5__["ConstantService"]])
    ], RegisterPage);
    return RegisterPage;
}());



/***/ }),

/***/ "./src/app/session/session.module.ts":
/*!*******************************************!*\
  !*** ./src/app/session/session.module.ts ***!
  \*******************************************/
/*! exports provided: SessionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SessionPageModule", function() { return SessionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _session_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./session.page */ "./src/app/session/session.page.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _register_register_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./register/register.page */ "./src/app/session/register/register.page.ts");
/* harmony import */ var _forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./forgot-password/forgot-password.page */ "./src/app/session/forgot-password/forgot-password.page.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./home/home.component */ "./src/app/session/home/home.component.ts");
/* harmony import */ var _login_login_page__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./login/login.page */ "./src/app/session/login/login.page.ts");













var routes = [
    {
        path: '',
        component: _home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"]
    },
    {
        path: 'register',
        component: _register_register_page__WEBPACK_IMPORTED_MODULE_9__["RegisterPage"]
    }, {
        path: 'forgot-password',
        component: _forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_10__["ForgotPasswordPage"]
    }, {
        path: 'login',
        component: _login_login_page__WEBPACK_IMPORTED_MODULE_12__["LoginPage"]
    }
];
var SessionPageModule = /** @class */ (function () {
    function SessionPageModule() {
    }
    SessionPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatExpansionModule"],
            ],
            declarations: [_session_page__WEBPACK_IMPORTED_MODULE_6__["SessionPage"],
                _register_register_page__WEBPACK_IMPORTED_MODULE_9__["RegisterPage"],
                _forgot_password_forgot_password_page__WEBPACK_IMPORTED_MODULE_10__["ForgotPasswordPage"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"],
                _login_login_page__WEBPACK_IMPORTED_MODULE_12__["LoginPage"]
            ]
        })
    ], SessionPageModule);
    return SessionPageModule;
}());



/***/ }),

/***/ "./src/app/session/session.page.html":
/*!*******************************************!*\
  !*** ./src/app/session/session.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>session</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/session/session.page.scss":
/*!*******************************************!*\
  !*** ./src/app/session/session.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Nlc3Npb24vc2Vzc2lvbi5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/session/session.page.ts":
/*!*****************************************!*\
  !*** ./src/app/session/session.page.ts ***!
  \*****************************************/
/*! exports provided: SessionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SessionPage", function() { return SessionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SessionPage = /** @class */ (function () {
    function SessionPage() {
    }
    SessionPage.prototype.ngOnInit = function () {
    };
    SessionPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-session',
            template: __webpack_require__(/*! ./session.page.html */ "./src/app/session/session.page.html"),
            styles: [__webpack_require__(/*! ./session.page.scss */ "./src/app/session/session.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SessionPage);
    return SessionPage;
}());



/***/ })

}]);
//# sourceMappingURL=session-session-module.js.map