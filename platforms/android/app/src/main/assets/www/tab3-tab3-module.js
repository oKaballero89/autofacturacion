(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab3-tab3-module"],{

/***/ "./src/app/cfdi/detail/detail.component.html":
/*!***************************************************!*\
  !*** ./src/app/cfdi/detail/detail.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <img src=\"{{img}}\">\n</ion-content>\n"

/***/ }),

/***/ "./src/app/cfdi/detail/detail.component.scss":
/*!***************************************************!*\
  !*** ./src/app/cfdi/detail/detail.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NmZGkvZGV0YWlsL2RldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/cfdi/detail/detail.component.ts":
/*!*************************************************!*\
  !*** ./src/app/cfdi/detail/detail.component.ts ***!
  \*************************************************/
/*! exports provided: DetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailComponent", function() { return DetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var DetailComponent = /** @class */ (function () {
    function DetailComponent(navParams, modalController) {
        this.navParams = navParams;
        this.modalController = modalController;
        this.sliderOpts = {
            zoom: {
                maxRatio: 5
            }
        };
        this.img = this.navParams.get('pathUrl');
    }
    DetailComponent.prototype.ngOnInit = function () { };
    DetailComponent.prototype.close = function () {
        this.modalController.dismiss();
    };
    DetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detail',
            template: __webpack_require__(/*! ./detail.component.html */ "./src/app/cfdi/detail/detail.component.html"),
            styles: [__webpack_require__(/*! ./detail.component.scss */ "./src/app/cfdi/detail/detail.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], DetailComponent);
    return DetailComponent;
}());



/***/ }),

/***/ "./src/app/services/constant.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/constant.service.ts ***!
  \**********************************************/
/*! exports provided: ConstantService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConstantService", function() { return ConstantService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConstantService = /** @class */ (function () {
    function ConstantService() {
        this.isprodCnk = false;
        this.baseUrl = window.location.protocol + '//' + window.location.host;
        this.host = window.location.host;
        //public apiConnect = 'http://localhost/proveedores-api/web/app_dev.php/api/v1/';
        this.apiConnect = 'http://gas-api.ilemuria.mx/web/app.php/api/v1/';
        this.conektaKey = (this.isprodCnk) ? "--" : "key_Nm78wRyBzzUy6Zssjf2guZg";
        this.companyApp = 'c1e0ea8a-87d4-11e9-9e4b-659de6a89800';
    }
    ConstantService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConstantService);
    return ConstantService;
}());



/***/ }),

/***/ "./src/app/services/general.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/general.service.ts ***!
  \*********************************************/
/*! exports provided: GeneralService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneralService", function() { return GeneralService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constant_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./constant.service */ "./src/app/services/constant.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var GeneralService = /** @class */ (function () {
    function GeneralService(toast, constant, router, loadingCtrl) {
        this.toast = toast;
        this.constant = constant;
        this.router = router;
        this.loadingCtrl = loadingCtrl;
        this.api_url = this.constant.apiConnect;
    }
    GeneralService.prototype.msgAlert = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toast.create({
                            message: message,
                            duration: 2000,
                            position: 'top'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    GeneralService.prototype.outSession = function () {
        localStorage.clear();
        this.router.navigate(['/']);
    };
    GeneralService.prototype.loading = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: message,
                            showBackdrop: true
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    GeneralService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _constant_service__WEBPACK_IMPORTED_MODULE_2__["ConstantService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
    ], GeneralService);
    return GeneralService;
}());



/***/ }),

/***/ "./src/app/tab3/tab3.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.module.ts ***!
  \*************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab3.page */ "./src/app/tab3/tab3.page.ts");
/* harmony import */ var _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../cfdi/detail/detail.component */ "./src/app/cfdi/detail/detail.component.ts");








var Tab3PageModule = /** @class */ (function () {
    function Tab3PageModule() {
    }
    Tab3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([
                    { path: '', component: _tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"] },
                    { path: 'modalPreview', component: _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_7__["DetailComponent"] }
                ])
            ],
            declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"], _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_7__["DetailComponent"]]
        })
    ], Tab3PageModule);
    return Tab3PageModule;
}());



/***/ }),

/***/ "./src/app/tab3/tab3.page.html":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>  \n    <ion-toolbar color=\"baseApp\">\n      <ion-title  >Facturas  </ion-title>\n      <ion-buttons slot=\"end\">\n          <ion-button color=\"secondary\" [routerLink]=\"['/profile']\">\n            <ion-icon name=\"contact\" style=\"font-size: 2em; color:#fff\" ></ion-icon>\n          </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n  </ion-header> \n  \n  \n  <ion-content>\n    <ion-list *ngIf=\"load\">\n      <ion-item *ngFor=\"let row of rows\" >\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"8\"> \n              {{row.rfc_receptor}}\n               <!-- Total: {{ row.total | currency }}   -->\n               <br>\n              \n              <small style=\"color: #000;\"> Total: {{ row.total | currency }} </small> \n              <small style=\"color: #a9a9a9;margin-left:10px\"> {{row.fecha | date: 'dd/MM/yyyy' }} </small> \n\n            </ion-col>\n            <ion-col size=\"4\" style=\"text-align: right\" #myImage (click)=\"presentModal(row.pdf)\"> \n              <div class=\"thumbPdf\" [style.backgroundImage]=\"'url('+ invoicePdf(row.pdf) +')'\" > </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-item>\n      \n    </ion-list>\n  \n    <!-- <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n      <ion-fab-button>\n        <ion-icon name=\"add\" (click)=\"addPay()\" ></ion-icon>\n        \n      </ion-fab-button>\n    </ion-fab> -->\n  </ion-content>\n  "

/***/ }),

/***/ "./src/app/tab3/tab3.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".thumbPdf {\n  background-position: center;\n  background-size: cover;\n  width: 40px;\n  height: 40px;\n  float: right; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3RhYjMvdGFiMy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSwyQkFBMkI7RUFDM0Isc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxZQUFZO0VBQ1osWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdGFiMy90YWIzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLnRodW1iUGRme1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICBmbG9hdDogcmlnaHQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/tab3/tab3.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab3/tab3.page.ts ***!
  \***********************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_tickets_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/tickets.service */ "./src/app/services/tickets.service.ts");
/* harmony import */ var _services_general_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/photo-viewer/ngx */ "./node_modules/@ionic-native/photo-viewer/ngx/index.js");
/* harmony import */ var _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../cfdi/detail/detail.component */ "./src/app/cfdi/detail/detail.component.ts");







var Tab3Page = /** @class */ (function () {
    function Tab3Page(ticketService, generalService, loading, photoViewer, modalCtrl) {
        this.ticketService = ticketService;
        this.generalService = generalService;
        this.loading = loading;
        this.photoViewer = photoViewer;
        this.modalCtrl = modalCtrl;
        this.rows = [];
        this.load = false;
        this.user = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.company = this.user.company;
        this.userId = this.user.id;
        this.user_token = this.user.session_token;
    }
    Tab3Page.prototype.ionViewWillEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.getInvoices();
        }, 500);
    };
    Tab3Page.prototype.getInvoices = function () {
        var _this = this;
        this.generalService.loading('Cargando');
        this.ticketService.getListInvoices(this.userId, this.user_token).subscribe(function (res) {
            _this.loading.dismiss();
            _this.load = true;
            _this.rows = res.msg;
        }, function (err) {
            console.log(err);
            _this.loading.dismiss();
        });
    };
    Tab3Page.prototype.invoicePdf = function (urlPdf) {
        var exploteStr = urlPdf.split('.pdf');
        return exploteStr[0] + '.jpg';
    };
    Tab3Page.prototype.detailInvoice = function (pdf) {
        var _this = this;
        var urlPath = this.invoicePdf(pdf);
        setTimeout(function () {
            console.log(': debe abrir url ::', urlPath);
            _this.photoViewer.show(urlPath);
        }, 500);
        //this.photoViewer.show('https://mysite.com/path/to/image.jpg');
    };
    Tab3Page.prototype.presentModal = function (pdf) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var urlPath, modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        urlPath = this.invoicePdf(pdf);
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_6__["DetailComponent"],
                                componentProps: { 'pathUrl': urlPath }
                            })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    Tab3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tab3',
            template: __webpack_require__(/*! ./tab3.page.html */ "./src/app/tab3/tab3.page.html"),
            styles: [__webpack_require__(/*! ./tab3.page.scss */ "./src/app/tab3/tab3.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_tickets_service__WEBPACK_IMPORTED_MODULE_2__["TicketsService"], _services_general_service__WEBPACK_IMPORTED_MODULE_3__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_5__["PhotoViewer"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]])
    ], Tab3Page);
    return Tab3Page;
}());



/***/ })

}]);
//# sourceMappingURL=tab3-tab3-module.js.map