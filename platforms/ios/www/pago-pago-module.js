(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pago-pago-module"],{

/***/ "./src/app/cfdi/detail/detail.component.html":
/*!***************************************************!*\
  !*** ./src/app/cfdi/detail/detail.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>  \n  <ion-toolbar color=\"baseApp\">\n    <ion-buttons slot=\"start\">\n      <ion-icon name=\"arrow-round-back\" (click)=\"close()\" style=\"font-size: 25px\"></ion-icon>\n    </ion-buttons>\n    \n  </ion-toolbar>\n  \n</ion-header> \n<ion-content>\n  <img src=\"{{img}}\">\n</ion-content>\n"

/***/ }),

/***/ "./src/app/cfdi/detail/detail.component.scss":
/*!***************************************************!*\
  !*** ./src/app/cfdi/detail/detail.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NmZGkvZGV0YWlsL2RldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/cfdi/detail/detail.component.ts":
/*!*************************************************!*\
  !*** ./src/app/cfdi/detail/detail.component.ts ***!
  \*************************************************/
/*! exports provided: DetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailComponent", function() { return DetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var DetailComponent = /** @class */ (function () {
    function DetailComponent(navParams, modalController) {
        this.navParams = navParams;
        this.modalController = modalController;
        this.sliderOpts = {
            zoom: {
                maxRatio: 5
            }
        };
        this.img = this.navParams.get('pathUrl');
    }
    DetailComponent.prototype.ngOnInit = function () { };
    DetailComponent.prototype.close = function () {
        this.modalController.dismiss();
    };
    DetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detail',
            template: __webpack_require__(/*! ./detail.component.html */ "./src/app/cfdi/detail/detail.component.html"),
            styles: [__webpack_require__(/*! ./detail.component.scss */ "./src/app/cfdi/detail/detail.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], DetailComponent);
    return DetailComponent;
}());



/***/ }),

/***/ "./src/app/invoices/send-invoice/send-invoice.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/invoices/send-invoice/send-invoice.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\" >\n    <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n      <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n    </ion-buttons>\n    <ion-title>Facturar </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <h3 style=\"text-align: center;\">Monto de facturación</h3>\n  <ion-grid>\n    <ion-row style=\"text-align: end\">\n      <ion-col size=\"8\">\n        <strong> Subtotal:</strong>\n      </ion-col>\n      <ion-col size=\"4\">\n        <label>{{ticket.subtotal | currency }}</label>\n      </ion-col>\n    </ion-row>\n\n    <ion-row style=\"text-align: end\">\n      <ion-col size=\"8\">\n        <strong> IVA:</strong>\n      </ion-col>\n      <ion-col size=\"4\">\n        <label>{{ticket.iva | currency }}</label>\n      </ion-col>\n    </ion-row>\n\n    <ion-row style=\"text-align: end\">\n      <ion-col size=\"8\">\n        <strong> Total:</strong>\n      </ion-col>\n      <ion-col size=\"4\">\n        <label>{{ticket.total | currency }}</label>\n      </ion-col>\n    </ion-row>\n\n  </ion-grid>\n  \n  \n<ion-list>\n  <ion-radio-group>\n    <ion-list-header>\n      <ion-label>Selecciona RFC para facturar</ion-label>\n    </ion-list-header>\n    <ion-item end>\n      <ion-button color=\"primary\" expand=\"block\"  (click)=\"addRfc()\" style=\"background: #3399CC; color:#fff\">\n        <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Agregar RFC\n      </ion-button>\n    </ion-item>\n    <ion-item *ngFor=\"let row of rows\">\n      <ion-label>\n        {{ row.razon_social }} <br> \n        {{ row.rfc }}\n      </ion-label>\n      <ion-radio slot=\"start\" value=\"{{row.id}}\" [(ngModel)]=\"rfcSelected\" (ionSelect)=\"showValue(row)\"  ></ion-radio>\n    </ion-item>\n\n  </ion-radio-group>\n</ion-list>\n\n\n</ion-content>\n\n<ion-footer class=\"footerButton\">\n  <ion-button color=\"primary\" expand=\"block\"  (click)=\"sendToInvoce()\" style=\"background: #3399CC; color:#fff\">\n    <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Facturar\n  </ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/invoices/send-invoice/send-invoice.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/invoices/send-invoice/send-invoice.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".divider {\n  border-left: 1px solid #7f7f7f;\n  border-right: 1px solid #7f7f7f;\n  border-bottom: 1px solid #7f7f7f; }\n\n.text-fin-ticket {\n  font-size: 12px; }\n\n.selectCustom {\n  border: 1px solid #3399CC; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL2ludm9pY2VzL3NlbmQtaW52b2ljZS9zZW5kLWludm9pY2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw4QkFBOEI7RUFDOUIsK0JBQStCO0VBQy9CLGdDQUFnQyxFQUFBOztBQUdwQztFQUNJLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSx5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ludm9pY2VzL3NlbmQtaW52b2ljZS9zZW5kLWludm9pY2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGl2aWRlcntcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICM3ZjdmN2Y7XG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzdmN2Y3ZjtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzdmN2Y3Zjtcbn1cblxuLnRleHQtZmluLXRpY2tldHtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG5cbi5zZWxlY3RDdXN0b217XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzMzOTlDQztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/invoices/send-invoice/send-invoice.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/invoices/send-invoice/send-invoice.component.ts ***!
  \*****************************************************************/
/*! exports provided: SendInvoiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendInvoiceComponent", function() { return SendInvoiceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/tickets.service */ "./src/app/services/tickets.service.ts");
/* harmony import */ var src_app_pago_add_rfc_add_rfc_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/pago/add-rfc/add-rfc.component */ "./src/app/pago/add-rfc/add-rfc.component.ts");







var SendInvoiceComponent = /** @class */ (function () {
    function SendInvoiceComponent(userService, modalCtrl, generalService, ticketService, navParams, loading) {
        var _this = this;
        this.userService = userService;
        this.modalCtrl = modalCtrl;
        this.generalService = generalService;
        this.ticketService = ticketService;
        this.navParams = navParams;
        this.loading = loading;
        this.rows = [];
        this.slideOpts = {
            initialSlide: 1,
            speed: 400
        };
        var b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.userId = b['id'];
        this.userToken = b['session_token'];
        this.company = b.company;
        this.code = this.navParams.get('ticket');
        console.log('debe facturar');
        setTimeout(function () {
            _this.getRfcs();
            _this.getTicketInfo();
        }, 500);
    }
    SendInvoiceComponent_1 = SendInvoiceComponent;
    SendInvoiceComponent.prototype.getRfcs = function () {
        var _this = this;
        this.userService.listRfcs(this.userId, this.userToken).subscribe(function (res) {
            //this.rfcSelected = res.msg[0].id;
            _this.rows = res.msg;
            console.log(_this.rows);
        });
    };
    SendInvoiceComponent.prototype.addressData = function (data) {
        if (data.city) {
            return data.street + ' ' + data.num_ext + ' int.' + data.num_int + ' ' + data.colony + ' CP ' + data.postal_code + ' ' + data.city + ' ' + data.state;
        }
        else {
            return "Sin dirección ";
        }
    };
    SendInvoiceComponent.prototype.getTicketInfo = function () {
        var _this = this;
        var params = {
            "companyId": this.company.id,
            "noTicket": this.code
        };
        this.ticketService.searchTicket(params, this.userToken).subscribe(function (res) {
            if (res.code == 501) {
                _this.generalService.msgAlert(res.msg);
                _this.generalService.outSession();
                return;
            }
            else if (res.code == 200) {
                _this.ticket = res.msg;
            }
        });
    };
    SendInvoiceComponent.prototype.ngOnInit = function () { };
    SendInvoiceComponent.prototype.backTab = function () {
        this.modalCtrl.dismiss();
    };
    SendInvoiceComponent.prototype.sendInvoice = function (code) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: SendInvoiceComponent_1,
                            componentProps: { 'ticket': code }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (res) {
                            console.log(':: cerro modal', res);
                            if (res) {
                                // Pregunta si quiere facturar //
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SendInvoiceComponent.prototype.sendToInvoce = function () {
        var _this = this;
        if (!this.rfcSelected) {
            this.generalService.msgAlert('Debes seleccionar RFC de facturación');
            return;
        }
        this.generalService.loading('Facturando compra');
        var params = { rfcId: this.rfcUser };
        this.ticketService.sendToInvoice(this.userId, this.ticket.id, params, this.userToken).subscribe(function (res) {
            _this.loading.dismiss();
            if (res.code == 200) {
                _this.generalService.msgAlert('Tu factura ha sido generada correctamente');
                setTimeout(function () {
                    _this.modalCtrl.dismiss({ 'status': 'update' });
                }, 500);
            }
        }, function (err) {
            _this.loading.dismiss();
            _this.generalService.msgAlert('No se pudo facturar en este momento, intentalo más tarde');
        });
    };
    SendInvoiceComponent.prototype.showValue = function (valor) {
        this.rfcUser = valor.id;
        console.log(valor);
    };
    SendInvoiceComponent.prototype.addRfc = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: src_app_pago_add_rfc_add_rfc_component__WEBPACK_IMPORTED_MODULE_6__["AddRfcComponent"],
                            componentProps: {}
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (res) {
                            if (res.data) {
                                // Pregunta si quiere facturar //
                                _this.getRfcs();
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    var SendInvoiceComponent_1;
    SendInvoiceComponent = SendInvoiceComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-send-invoice',
            template: __webpack_require__(/*! ./send-invoice.component.html */ "./src/app/invoices/send-invoice/send-invoice.component.html"),
            styles: [__webpack_require__(/*! ./send-invoice.component.scss */ "./src/app/invoices/send-invoice/send-invoice.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__["GeneralService"], src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_5__["TicketsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
    ], SendInvoiceComponent);
    return SendInvoiceComponent;
}());



/***/ }),

/***/ "./src/app/pago/add-rfc/add-rfc.component.html":
/*!*****************************************************!*\
  !*** ./src/app/pago/add-rfc/add-rfc.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\" style=\"background: blue !important; color: #fff\">\n      <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n        <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n      </ion-buttons>\n    <ion-title  > {{title}} RFC </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n\n    <ion-row>\n      <ion-col>  \n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input required matInput placeholder=\"Razón social\" [(ngModel)]=\"invoiceData.razon_social\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col>          \n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input required matInput placeholder=\"RFC\" [(ngModel)]=\"invoiceData.RFC\" style=\"text-transform: uppercase\"  value=\"\">\n        </mat-form-field>          \n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      \n      <ion-col size=\"6\">\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n              <input matInput placeholder=\"País\" [(ngModel)]=\"invoiceData.pais\"  value=\"\">\n          </mat-form-field>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Estado\" [(ngModel)]=\"invoiceData.estado\"  value=\"\">\n        </mat-form-field>\n    </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\">\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Ciudad\" [(ngModel)]=\"invoiceData.ciudad\"  value=\"\">\n          </mat-form-field>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"Colonia\" [(ngModel)]=\"invoiceData.colonia\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"Calle\" [(ngModel)]=\"invoiceData.calle\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"CP\" [(ngModel)]=\"invoiceData.cp\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n      \n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"No. Int\" [(ngModel)]=\"invoiceData.noInt\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n\n      <ion-col size=\"6\">\n        <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n          <input matInput placeholder=\"No. Ext\" [(ngModel)]=\"invoiceData.noExt\"  value=\"\">\n        </mat-form-field>\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n</ion-content>\n\n<ion-footer class=\"footerButton\" >\n    <ion-button color=\"primary\" expand=\"block\"  (click)=\"addRfc()\" style=\"background: #3399CC; color:#fff\" *ngIf=\"title == 'Nuevo' \">\n      <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Agregar\n    </ion-button>\n\n    <ion-button color=\"primary\" expand=\"block\"  (click)=\"updatePassword()\" style=\"background: #3399CC; color:#fff\" *ngIf=\"title != 'Nuevo' \">\n      <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Actualizar\n    </ion-button>\n  </ion-footer>\n"

/***/ }),

/***/ "./src/app/pago/add-rfc/add-rfc.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/pago/add-rfc/add-rfc.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ28vYWRkLXJmYy9hZGQtcmZjLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pago/add-rfc/add-rfc.component.ts":
/*!***************************************************!*\
  !*** ./src/app/pago/add-rfc/add-rfc.component.ts ***!
  \***************************************************/
/*! exports provided: AddRfcComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddRfcComponent", function() { return AddRfcComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






var AddRfcComponent = /** @class */ (function () {
    function AddRfcComponent(generalService, userService, router, modalCtrl) {
        this.generalService = generalService;
        this.userService = userService;
        this.router = router;
        this.modalCtrl = modalCtrl;
        this.title = 'Nuevo';
        this.form = {};
        this.invoiceData = {};
        var b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.userId = b['id'];
        this.userToken = b['session_token'];
    }
    AddRfcComponent.prototype.ngOnInit = function () { };
    AddRfcComponent.prototype.addRfc = function () {
        var _this = this;
        if (!this.invoiceData.razon_social || this.invoiceData.razon_social == undefined) {
            this.generalService.msgAlert('Debes ingresar la razón social');
            return;
        }
        if (!this.invoiceData.RFC || this.invoiceData.RFC == undefined) {
            this.generalService.msgAlert('Debes ingresar RFC ');
            return;
        }
        this.userService.addRfc(this.invoiceData, this.userId, this.userToken).subscribe(function (res) {
            if (res.code == 200) {
                _this.generalService.msgAlert('RFC agregado correctamente');
                setTimeout(function () {
                    _this.modalCtrl.dismiss({ 'status': 'update' });
                    _this.invoiceData = {};
                }, 800);
            }
            else {
                _this.generalService.msgAlert(res.msg);
            }
            console.log(res);
        });
    };
    AddRfcComponent.prototype.backTab = function () {
        this.modalCtrl.dismiss();
    };
    AddRfcComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-rfc',
            template: __webpack_require__(/*! ./add-rfc.component.html */ "./src/app/pago/add-rfc/add-rfc.component.html"),
            styles: [__webpack_require__(/*! ./add-rfc.component.scss */ "./src/app/pago/add-rfc/add-rfc.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_general_service__WEBPACK_IMPORTED_MODULE_2__["GeneralService"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]])
    ], AddRfcComponent);
    return AddRfcComponent;
}());



/***/ }),

/***/ "./src/app/pago/pago.module.ts":
/*!*************************************!*\
  !*** ./src/app/pago/pago.module.ts ***!
  \*************************************/
/*! exports provided: PagoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagoPageModule", function() { return PagoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pago_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pago.page */ "./src/app/pago/pago.page.ts");
/* harmony import */ var _tickets_tickets_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tickets/tickets.page */ "./src/app/pago/tickets/tickets.page.ts");
/* harmony import */ var _ticket_ticket_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ticket/ticket.component */ "./src/app/pago/ticket/ticket.component.ts");
/* harmony import */ var _ticket_pay_ticket_pay_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ticket-pay/ticket-pay.component */ "./src/app/pago/ticket-pay/ticket-pay.component.ts");
/* harmony import */ var _invoices_send_invoice_send_invoice_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../invoices/send-invoice/send-invoice.component */ "./src/app/invoices/send-invoice/send-invoice.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _add_rfc_add_rfc_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./add-rfc/add-rfc.component */ "./src/app/pago/add-rfc/add-rfc.component.ts");
/* harmony import */ var _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../cfdi/detail/detail.component */ "./src/app/cfdi/detail/detail.component.ts");














var routes = [
    { path: '', component: _pago_page__WEBPACK_IMPORTED_MODULE_6__["PagoPage"] },
    { path: 'readTicket', component: _tickets_tickets_page__WEBPACK_IMPORTED_MODULE_7__["TicketsPage"] },
    { path: 'modalTicket-pay', component: _ticket_pay_ticket_pay_component__WEBPACK_IMPORTED_MODULE_9__["TicketPayComponent"] },
    { path: 'modalInvoice-pay', component: _invoices_send_invoice_send_invoice_component__WEBPACK_IMPORTED_MODULE_10__["SendInvoiceComponent"] },
    { path: 'modalRfc', component: _add_rfc_add_rfc_component__WEBPACK_IMPORTED_MODULE_12__["AddRfcComponent"] },
    { path: 'detailTicket', component: _ticket_ticket_component__WEBPACK_IMPORTED_MODULE_8__["TicketComponent"] },
    { path: 'detailInvoice', component: _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_13__["DetailComponent"] }
];
var PagoPageModule = /** @class */ (function () {
    function PagoPageModule() {
    }
    PagoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatExpansionModule"]
            ],
            declarations: [_pago_page__WEBPACK_IMPORTED_MODULE_6__["PagoPage"],
                _tickets_tickets_page__WEBPACK_IMPORTED_MODULE_7__["TicketsPage"],
                _ticket_pay_ticket_pay_component__WEBPACK_IMPORTED_MODULE_9__["TicketPayComponent"],
                _invoices_send_invoice_send_invoice_component__WEBPACK_IMPORTED_MODULE_10__["SendInvoiceComponent"],
                _add_rfc_add_rfc_component__WEBPACK_IMPORTED_MODULE_12__["AddRfcComponent"],
                _ticket_ticket_component__WEBPACK_IMPORTED_MODULE_8__["TicketComponent"],
                _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_13__["DetailComponent"]
            ]
        })
    ], PagoPageModule);
    return PagoPageModule;
}());



/***/ }),

/***/ "./src/app/pago/pago.page.html":
/*!*************************************!*\
  !*** ./src/app/pago/pago.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>  \n    <ion-toolbar color=\"baseApp\">\n      <ion-buttons slot=\"start\">\n        <ion-menu-button></ion-menu-button>\n      </ion-buttons>\n\n      <ion-title  >Pagos  </ion-title>\n      \n    </ion-toolbar>\n  </ion-header> \n  \n  \n  <ion-content>\n    <ion-list *ngIf=\"load\">\n      <ion-item *ngFor=\"let row of rows\" >\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"8\" (click)=\"ticketDetail(row.ticketId)\"> \n               Total: {{ row.total | currency }}  <br>\n              <small style=\"color: #a9a9a9;\"> {{row.fecha | date: 'dd/MM/yyyy' }} </small> \n            </ion-col>\n            <ion-col size=\"4\" style=\"text-align: right\"> \n\n              <div class=\"contentInvoice\" *ngIf=\"!row.pdf\" (click)=\"Facturar(row.codeTicket)\"> \n                <ion-icon name=\"document\" class=\"colorClick\"></ion-icon>\n                <br>\n                <span style=\"font-size: 10px\">  Facturar </span>\n              </div>\n\n              <div  *ngIf=\"row.pdf\" class=\"contentInvoice\" [style.backgroundImage]=\"'url('+ parsePdfXml(row) +')'\" \n              style=\"background-size: cover;\" (click)=\"openInvoice(row)\" ></div>\n                <!-- <ion-badge *ngIf=\"row.status == 'success' \" color=\"success\">Pagado</ion-badge>\n                <ion-badge *ngIf=\"row.status == 'failed' \" color=\"danger\">Rechazado</ion-badge> -->\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-item>\n      \n    </ion-list>\n  \n    <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n      <ion-fab-button>\n        <ion-icon name=\"add\" (click)=\"addPay()\" ></ion-icon>\n        \n      </ion-fab-button>\n    </ion-fab>\n  </ion-content>\n  "

/***/ }),

/***/ "./src/app/pago/pago.page.scss":
/*!*************************************!*\
  !*** ./src/app/pago/pago.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".contentInvoice {\n  border: 1px solid #3399CC;\n  text-align: center;\n  padding: 4px;\n  border-radius: 10px;\n  width: 60px;\n  height: 60px;\n  margin-left: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3BhZ28vcGFnby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFDWixpQkFBaUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ28vcGFnby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGVudEludm9pY2V7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzMzOTlDQztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogNHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgd2lkdGg6IDYwcHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pago/pago.page.ts":
/*!***********************************!*\
  !*** ./src/app/pago/pago.page.ts ***!
  \***********************************/
/*! exports provided: PagoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagoPage", function() { return PagoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_tickets_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/tickets.service */ "./src/app/services/tickets.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pago_ticket_ticket_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pago/ticket/ticket.component */ "./src/app/pago/ticket/ticket.component.ts");
/* harmony import */ var _services_general_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var _tickets_tickets_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tickets/tickets.page */ "./src/app/pago/tickets/tickets.page.ts");
/* harmony import */ var _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../cfdi/detail/detail.component */ "./src/app/cfdi/detail/detail.component.ts");
/* harmony import */ var _invoices_send_invoice_send_invoice_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../invoices/send-invoice/send-invoice.component */ "./src/app/invoices/send-invoice/send-invoice.component.ts");










var PagoPage = /** @class */ (function () {
    function PagoPage(router, tickets, modalCtrl, generalService, loading) {
        this.router = router;
        this.tickets = tickets;
        this.modalCtrl = modalCtrl;
        this.generalService = generalService;
        this.loading = loading;
        this.rows = [];
        this.load = false;
        this.user = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.company = this.user.company;
        this.userId = this.user.id;
        this.user_token = this.user.session_token;
    }
    PagoPage.prototype.ngOnInit = function () {
    };
    PagoPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.payments();
        }, 500);
    };
    // addPay(){
    //   this.router.navigate(['ticket/readTicket']);  
    // }
    PagoPage.prototype.payments = function () {
        var _this = this;
        this.generalService.loading('Cargando');
        this.tickets.paymentsTickets(this.userId, this.user_token).subscribe(function (res) {
            _this.loading.dismiss();
            _this.load = true;
            _this.rows = res.msg;
            console.log(':: ******** ::', res);
        }, function (err) {
            _this.loading.dismiss();
            if (err.status == 401) {
                //this.generalService.outSession();
            }
            //this.generalService.outSession();
        });
    };
    PagoPage.prototype.ticketDetail = function (ticketId) {
        var _this = this;
        this.tickets.getTicket(this.userId, ticketId, this.user_token).subscribe(function (res) {
            console.log('debe entrar a modal');
            _this.presentModal(res.msg);
        });
    };
    PagoPage.prototype.presentModal = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _pago_ticket_ticket_component__WEBPACK_IMPORTED_MODULE_5__["TicketComponent"],
                            componentProps: { 'msg': params, type: 'detail' }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PagoPage.prototype.openInvoice = function (info) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var img, modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        img = this.parsePdfXml(info);
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: _cfdi_detail_detail_component__WEBPACK_IMPORTED_MODULE_8__["DetailComponent"],
                                componentProps: { 'pathUrl': img }
                            })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PagoPage.prototype.addPay = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _tickets_tickets_page__WEBPACK_IMPORTED_MODULE_7__["TicketsPage"],
                            componentProps: {}
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (res) {
                            if (res) {
                                // Pregunta si quiere facturar //      
                                if (res.data) {
                                    //this.router.navigate(['tabs/tab2']);  
                                    _this.payments();
                                }
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PagoPage.prototype.parsePdfXml = function (info) {
        var PathPdf = info.pdf.split(".pdf");
        var img = PathPdf[0] + '.jpg';
        return img;
    };
    //SendInvoiceComponent
    PagoPage.prototype.Facturar = function (code) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(':: debe enviar ::');
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: _invoices_send_invoice_send_invoice_component__WEBPACK_IMPORTED_MODULE_9__["SendInvoiceComponent"],
                                componentProps: { 'ticket': code }
                            })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (res) {
                            if (res) {
                                // Pregunta si quiere facturar //      
                                if (res.data) {
                                    //this.router.navigate(['tabs/tab2']);  
                                    _this.payments();
                                }
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PagoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pago',
            template: __webpack_require__(/*! ./pago.page.html */ "./src/app/pago/pago.page.html"),
            styles: [__webpack_require__(/*! ./pago.page.scss */ "./src/app/pago/pago.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_tickets_service__WEBPACK_IMPORTED_MODULE_3__["TicketsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _services_general_service__WEBPACK_IMPORTED_MODULE_6__["GeneralService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]])
    ], PagoPage);
    return PagoPage;
}());



/***/ }),

/***/ "./src/app/pago/ticket-pay/ticket-pay.component.html":
/*!***********************************************************!*\
  !*** ./src/app/pago/ticket-pay/ticket-pay.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\" >\n    <ion-buttons slot=\"start\" style=\"position: absolute;\" *ngIf=\"state == 'pay' \">\n      <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n    </ion-buttons>\n    <ion-title>Ticket </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-grid *ngIf=\"state == 'pay' \">\n    <ion-row>\n      <ion-col>\n          <ion-select placeholder=\"Seleccione tarjeta\" class=\"selectCustom\" [(ngModel)]=\"cardPay\" >\n            <ion-select-option *ngFor=\"let row of cards\" value=\"{{row.id}}\"  >XXXX-{{row.number}}</ion-select-option>\n            <!-- <ion-select-option value=\"new\">Agregar tarjeta</ion-select-option> -->\n          </ion-select>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"head-table\"> \n      <ion-col size=\"4\" class=\"divider\"> <b> Catidad</b> </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> <b>Producto</b> </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> <b>Importe</b>  </ion-col>\n    </ion-row>\n\n    <ion-row *ngFor=\"let row of conceptos\"> \n      <ion-col size=\"4\" class=\"divider\"> {{ row.cantidad }} </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> {{row.descripcion}} </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> {{ row.importe | currency }}  </ion-col>\n    </ion-row>\n\n  <!--  Datos de ticket final -->\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>Subtotal</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\">  {{ subtotal | currency }} </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>I.V.A.</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\"> {{ iva | currency }}</ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>Total</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\"> {{ total | currency }} </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid *ngIf=\"state=='payed' \">\n    <ion-row>\n      <ion-col>\n        <h2 style=\" text-align: center;color: #919191;\">Gracias por tu preferencia </h2>\n      </ion-col>\n    </ion-row>\n\n    <ion-row> \n      <ion-col style=\"text-align: center\">\n          <ion-icon name=\"checkmark-circle\" style=\"color:green; font-size:8em\"></ion-icon>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n        <ion-col>\n          <p style=\" text-align: center;color: #919191;\"> ¿Deseas facturar esta compra?</p>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size=\"6\" style=\"text-align: center;\">\n            <button mat-stroked-button color=\"warn\" style=\"width:10em\" (click)=\"invoice('no')\">No</button>\n        </ion-col>\n\n        <ion-col size=\"6\" style=\"text-align: center;\">\n            <button mat-stroked-button color=\"primary\" style=\"width:10em\" (click)=\"invoice('si')\">Si</button>\n        </ion-col>\n      </ion-row>\n  </ion-grid>\n\n</ion-content>\n\n<ion-footer class=\"footerButton\" *ngIf=\"state == 'pay' \">\n  <ion-button color=\"primary\" expand=\"block\"  (click)=\"PayTicket()\" style=\"background: #3399CC; color:#fff\">\n    <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Pagar\n  </ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pago/ticket-pay/ticket-pay.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/pago/ticket-pay/ticket-pay.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".head-table {\n  background-color: #3399CC;\n  color: #fff; }\n\n.divider {\n  border-left: 1px solid #7f7f7f;\n  border-right: 1px solid #7f7f7f;\n  border-bottom: 1px solid #7f7f7f; }\n\n.text-fin-ticket {\n  font-size: 12px; }\n\n.selectCustom {\n  border: 1px solid #3399CC; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3BhZ28vdGlja2V0LXBheS90aWNrZXQtcGF5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQXlCO0VBQ3pCLFdBQ0osRUFBQTs7QUFFQTtFQUNJLDhCQUE4QjtFQUM5QiwrQkFBK0I7RUFDL0IsZ0NBQWdDLEVBQUE7O0FBR3BDO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLHlCQUF5QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnby90aWNrZXQtcGF5L3RpY2tldC1wYXkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZC10YWJsZXtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzM5OUNDO1xuICAgIGNvbG9yOiAjZmZmXG59XG5cbi5kaXZpZGVye1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgIzdmN2Y3ZjtcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjN2Y3ZjdmO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjN2Y3ZjdmO1xufVxuXG4udGV4dC1maW4tdGlja2V0e1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLnNlbGVjdEN1c3RvbXtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzM5OUNDO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pago/ticket-pay/ticket-pay.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/pago/ticket-pay/ticket-pay.component.ts ***!
  \*********************************************************/
/*! exports provided: TicketPayComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketPayComponent", function() { return TicketPayComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/tickets.service */ "./src/app/services/tickets.service.ts");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");






var TicketPayComponent = /** @class */ (function () {
    function TicketPayComponent(params, userServices, ticketService, modalCtrl, generalService, loading) {
        var _this = this;
        this.params = params;
        this.userServices = userServices;
        this.ticketService = ticketService;
        this.modalCtrl = modalCtrl;
        this.generalService = generalService;
        this.loading = loading;
        this.conceptos = [];
        this.cards = [];
        this.state = 'pay';
        var b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.userId = b['id'];
        this.userToken = b['session_token'];
        this.company = b.company;
        this.user_token = b.session_token;
        var data = this.params.get('msg');
        var type = this.params.get('type');
        this.conceptos = data.conceptos;
        this.subtotal = data.subtotal;
        this.total = data.total;
        this.iva = data.iva;
        this.ticketId = data.id;
        if (type != 'pay') {
            setTimeout(function () {
                _this.typeView = type;
            }, 300);
        }
        setTimeout(function () {
            _this.getList();
        }, 200);
    }
    TicketPayComponent.prototype.ngOnInit = function () { };
    TicketPayComponent.prototype.PayTicket = function () {
        var _this = this;
        console.log('terjeta::', this.cardPay);
        if (!this.cardPay || this.cardPay == undefined || this.cardPay == '') {
            this.generalService.msgAlert('Debes seleccionr una tarjeta de pago');
            return;
        }
        this.generalService.loading('Procesando pago');
        this.ticketService.payTicket({ cardId: this.cardPay }, this.userId, this.ticketId, this.userToken).subscribe(function (res) {
            _this.loading.dismiss();
            if (res.code == 200) {
                _this.state = 'payed';
            }
            else {
                if (res.code == 501) {
                    _this.generalService.msgAlert(res.msg);
                }
                else if (res.code == 500) {
                    _this.generalService.msgAlert(res.message);
                }
            }
        }, function (err) {
            // errores que retorna coneckta caen aquí
            _this.loading.dismiss();
            _this.generalService.msgAlert(err.error.message);
        });
    };
    TicketPayComponent.prototype.getList = function () {
        var _this = this;
        this.userServices.listCards(this.userId, this.userToken).subscribe(function (res) {
            _this.cards = res.msg;
        });
    };
    TicketPayComponent.prototype.backTab = function () {
        this.modalCtrl.dismiss({});
    };
    TicketPayComponent.prototype.invoice = function (param) {
        if (param == 'si') {
            this.modalCtrl.dismiss({ status: 'invoice' });
        }
        else {
            this.modalCtrl.dismiss({ status: 'update' });
        }
    };
    TicketPayComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ticket-pay',
            template: __webpack_require__(/*! ./ticket-pay.component.html */ "./src/app/pago/ticket-pay/ticket-pay.component.html"),
            styles: [__webpack_require__(/*! ./ticket-pay.component.scss */ "./src/app/pago/ticket-pay/ticket-pay.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_4__["TicketsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], src_app_services_general_service__WEBPACK_IMPORTED_MODULE_5__["GeneralService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]])
    ], TicketPayComponent);
    return TicketPayComponent;
}());



/***/ }),

/***/ "./src/app/pago/ticket/ticket.component.html":
/*!***************************************************!*\
  !*** ./src/app/pago/ticket/ticket.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\" >\n    <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n      <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n    </ion-buttons>\n    <ion-title>Ticket </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n          <ion-select placeholder=\"Seleccione tarjeta\" class=\"selectCustom\" [(ngModel)]=\"cardPay\" *ngIf=\"typeView == 'pay' \" >\n            <ion-select-option *ngFor=\"let row of cards\" value=\"{{row.id}}\"  >XXXX-{{row.number}}</ion-select-option>\n            <ion-select-option value=\"new\">Agregar tarjeta</ion-select-option>\n          </ion-select>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"head-table\"> \n      <ion-col size=\"4\" class=\"divider\"> <b> Catidad</b> </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> <b>Producto</b> </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> <b>Importe</b>  </ion-col>\n    </ion-row>\n\n    <ion-row *ngFor=\"let row of conceptos\"> \n      <ion-col size=\"4\" class=\"divider\"> {{ row.cantidad }} </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> {{row.descripcion}} </ion-col>\n      <ion-col size=\"4\" class=\"divider\"> {{ row.importe | currency }}  </ion-col>\n    </ion-row>\n\n  <!--  Datos de ticket final -->\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>Subtotal</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\">  {{ subtotal | currency }} </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>I.V.A.</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\"> {{ iva | currency }}</ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col size=\"6\" > </ion-col>\n      <ion-col size=\"2\" class=\"text-fin-ticket\" > <b>Total</b> </ion-col>\n      <ion-col size=\"4\" class=\"text-fin-ticket\"> {{ total | currency }} </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-footer class=\"footerButton\" *ngIf=\"typeView == 'pay' \">\n  <ion-button color=\"primary\" expand=\"block\"  (click)=\"PayTicket()\" style=\"background: #3399CC; color:#fff\">\n    <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Pagar\n  </ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/pago/ticket/ticket.component.scss":
/*!***************************************************!*\
  !*** ./src/app/pago/ticket/ticket.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".head-table {\n  background-color: #3399CC;\n  color: #fff; }\n\n.divider {\n  border-left: 1px solid #7f7f7f;\n  border-right: 1px solid #7f7f7f;\n  border-bottom: 1px solid #7f7f7f; }\n\n.text-fin-ticket {\n  font-size: 12px; }\n\n.selectCustom {\n  border: 1px solid #3399CC; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3BhZ28vdGlja2V0L3RpY2tldC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUF5QjtFQUN6QixXQUNKLEVBQUE7O0FBRUE7RUFDSSw4QkFBOEI7RUFDOUIsK0JBQStCO0VBQy9CLGdDQUFnQyxFQUFBOztBQUdwQztFQUNJLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSx5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ28vdGlja2V0L3RpY2tldC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkLXRhYmxle1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMzk5Q0M7XG4gICAgY29sb3I6ICNmZmZcbn1cblxuLmRpdmlkZXJ7XG4gICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjN2Y3ZjdmO1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICM3ZjdmN2Y7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM3ZjdmN2Y7XG59XG5cbi50ZXh0LWZpbi10aWNrZXR7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uc2VsZWN0Q3VzdG9te1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMzMzk5Q0M7XG59Il19 */"

/***/ }),

/***/ "./src/app/pago/ticket/ticket.component.ts":
/*!*************************************************!*\
  !*** ./src/app/pago/ticket/ticket.component.ts ***!
  \*************************************************/
/*! exports provided: TicketComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketComponent", function() { return TicketComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/tickets.service */ "./src/app/services/tickets.service.ts");





var TicketComponent = /** @class */ (function () {
    function TicketComponent(params, userServices, ticketService, modalCtrl) {
        var _this = this;
        this.params = params;
        this.userServices = userServices;
        this.ticketService = ticketService;
        this.modalCtrl = modalCtrl;
        this.conceptos = [];
        this.cards = [];
        var b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.userId = b['id'];
        this.userToken = b['session_token'];
        this.company = b.company;
        this.user_token = b.session_token;
        var data = this.params.get('msg');
        var type = this.params.get('type');
        this.conceptos = data.conceptos;
        this.subtotal = data.subtotal;
        this.total = data.total;
        this.iva = data.iva;
        this.ticketId = data.id;
        if (type != 'pay') {
            setTimeout(function () {
                _this.typeView = type;
            }, 300);
        }
        //console.log('::: data', data);
        setTimeout(function () {
            _this.getList();
        }, 200);
    }
    TicketComponent.prototype.ngOnInit = function () { };
    TicketComponent.prototype.PayTicket = function () {
        this.ticketService.payTicket({ cardId: this.cardPay }, this.userId, this.ticketId, this.userToken).subscribe(function (res) {
            console.log(res);
        });
    };
    TicketComponent.prototype.getList = function () {
        var _this = this;
        this.userServices.listCards(this.userId, this.userToken).subscribe(function (res) {
            _this.cards = res.msg;
        });
    };
    TicketComponent.prototype.backTab = function () {
        this.modalCtrl.dismiss({});
    };
    TicketComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ticket',
            template: __webpack_require__(/*! ./ticket.component.html */ "./src/app/pago/ticket/ticket.component.html"),
            styles: [__webpack_require__(/*! ./ticket.component.scss */ "./src/app/pago/ticket/ticket.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_4__["TicketsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], TicketComponent);
    return TicketComponent;
}());



/***/ }),

/***/ "./src/app/pago/tickets/tickets.page.html":
/*!************************************************!*\
  !*** ./src/app/pago/tickets/tickets.page.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"baseApp\">\n    \n    <ion-buttons slot=\"start\" style=\"position: absolute;\" >\n      <ion-icon name=\"arrow-round-back\" style=\"font-size: 25px;\" (click)=\"backTab()\"></ion-icon>\n    </ion-buttons>\n\n    <ion-title>Realizar nuevo pago</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [class.show-qr-scanner]=\"isOn\">\n  <ion-grid style=\"margin-top:3em\">\n    <ion-row>\n      <ion-col  >\n        <div class=\"textScan\">\n          Para realizar el pago se tus productos scanea el QR del ticket \n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row >\n      <ion-col style=\"text-align: center;\" (click)=\"scann()\">\n         <ion-icon name=\"qr-scanner\" class=\"baseColor\"  style=\"font-size: 8em\" ></ion-icon>\n      </ion-col>\n      \n    </ion-row>\n\n    <ion-row>\n      <ion-col >\n          <ion-button color=\"primary\" expand=\"block\"  (click)=\"scann()\" style=\"background: #3399CC; color:#fff\">\n            <ion-icon name=\"ios-qr-scanner-outline\"  > </ion-icon>  escanear Qr\n          </ion-button>\n      </ion-col>\n    </ion-row>\n\n    <ion-row style=\"margin-top:1em\">\n      <ion-col >\n          <div class=\"textScan\">\n            o  ingresa el código que aparece en el ticket \n          </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col align-self-center>        \n        <ion-input placeholder=\"XXXXXXXX\" class=\"inputCode\" style=\"border: none;height: 100%; width: 100%; border: 1px solid #3399CC;\" \n        (keyup) = \"searchTicket($event)\"  [(ngModel)]=\"inputTicket\" ></ion-input> \n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col >\n          <ion-button color=\"primary\" expand=\"block\"  (click)=\"activeSearch(inputTicket)\" style=\"background: #3399CC; color:#fff\">\n            <ion-icon name=\"ios-qr-scanner-outline\" > </ion-icon>  Buscar\n          </ion-button>\n      </ion-col>\n    </ion-row>\n\n\n  </ion-grid>\n\n\n    \n</ion-content>\n"

/***/ }),

/***/ "./src/app/pago/tickets/tickets.page.scss":
/*!************************************************!*\
  !*** ./src/app/pago/tickets/tickets.page.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inputCode {\n  border: 1px solid #e9e9e9;\n  color: #8e8e8e;\n  text-align: center;\n  font-size: 25px; }\n\n.textScan {\n  color: #9e9e9e;\n  font-size: 12px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vY2FiYWxsZXJvL0RvY3VtZW50cy9pb25pYy9hdXRvRmFjdC9zcmMvYXBwL3BhZ28vdGlja2V0cy90aWNrZXRzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxjQUFjO0VBQ2QsZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnby90aWNrZXRzL3RpY2tldHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmlucHV0Q29kZXtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTllOWU5O1xuICAgIGNvbG9yOiAjOGU4ZThlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDI1cHg7XG59XG5cbi50ZXh0U2NhbntcbiAgICBjb2xvcjogIzllOWU5ZTtcbiAgICBmb250LXNpemU6IDEycHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/pago/tickets/tickets.page.ts":
/*!**********************************************!*\
  !*** ./src/app/pago/tickets/tickets.page.ts ***!
  \**********************************************/
/*! exports provided: TicketsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketsPage", function() { return TicketsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/tickets.service */ "./src/app/services/tickets.service.ts");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ticket_pay_ticket_pay_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../ticket-pay/ticket-pay.component */ "./src/app/pago/ticket-pay/ticket-pay.component.ts");
/* harmony import */ var src_app_invoices_send_invoice_send_invoice_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/invoices/send-invoice/send-invoice.component */ "./src/app/invoices/send-invoice/send-invoice.component.ts");


//import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';






var TicketsPage = /** @class */ (function () {
    function TicketsPage(platform, ticketService, generalService, modalCtrl, router, loading) {
        var _this = this;
        this.platform = platform;
        this.ticketService = ticketService;
        this.generalService = generalService;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.loading = loading;
        this.isOn = false;
        this.user = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
        this.company = this.user.company;
        this.user_token = this.user.session_token;
        //                                                         this.inputTicket = 'kjND9U';
        this.platform.backButton.subscribeWithPriority(0, function () {
            document.getElementsByTagName("body")[0].style.opacity = "1";
            _this.qrScan.unsubscribe();
        });
    }
    TicketsPage.prototype.ngOnInit = function () {
        setTimeout(function () {
            // this.sendInvoice(this.inputTicket);  
        }, 1000);
    };
    TicketsPage.prototype.activeSearch = function (code) {
        var _this = this;
        this.codeInput = code;
        this.generalService.loading('Buscando ticket');
        var params = {
            "companyId": this.company.id,
            "noTicket": code
        };
        this.ticketService.searchTicket(params, this.user_token).subscribe(function (res) {
            _this.loading.dismiss();
            if (res.code == 501) {
                _this.generalService.msgAlert(res.msg);
            }
            else if (res.code == 200) {
                setTimeout(function () {
                    _this.openModal(res.msg);
                }, 400);
            }
        });
    };
    /*
      scann(){
        // Optionally request the permission early
        this.qrScanner.prepare().then((status: QRScannerStatus) => {
          if(status.authorized){
            this.qrScanner.show();
            document.getElementsByTagName("body")[0].style.opacity = "0";
            this.qrScan = this.qrScanner.scan().subscribe((textFound) => {
              document.getElementsByTagName("body")[0].style.opacity = "1";
              this.qrScan.unsubscribe();
              // console.info(textFound);
              // console.log('::: debio scanear :::');
              this.activeSearch(textFound);
              setTimeout(() => {
                
              }, 20);
    
            }, err => {
              console.log(JSON.stringify(err));
            })
          }else if(status.denied){
          }
        });
      }*/
    TicketsPage.prototype.searchTicket = function (event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                if (event.keyCode == 13) {
                    console.log('debe buscar ', this.inputTicket);
                    this.inputTicket;
                    this.activeSearch(this.inputTicket);
                }
                return [2 /*return*/];
            });
        });
    };
    TicketsPage.prototype.presentModal = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('debe abrir la pinshi modal ', params);
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: _ticket_pay_ticket_pay_component__WEBPACK_IMPORTED_MODULE_6__["TicketPayComponent"],
                                componentProps: { 'msg': params }
                            })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    TicketsPage.prototype.openModal = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _ticket_pay_ticket_pay_component__WEBPACK_IMPORTED_MODULE_6__["TicketPayComponent"],
                            componentProps: { 'msg': params }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (res) {
                            console.log(':: cerro modal', res);
                            if (res.data.status == 'invoice') {
                                // Pregunta si quiere facturar //
                                _this.sendInvoice(_this.inputTicket);
                            }
                            else if (res.data.status == 'update') {
                                _this.backTab();
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TicketsPage.prototype.sendInvoice = function (code) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: src_app_invoices_send_invoice_send_invoice_component__WEBPACK_IMPORTED_MODULE_7__["SendInvoiceComponent"],
                            componentProps: { 'ticket': this.codeInput }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (res) {
                            if (res) {
                                // Pregunta si quiere facturar //      
                                if (res.data.status == 'update') {
                                    //this.router.navigate(['tabs/tab2']);  
                                    _this.modalCtrl.dismiss({ 'status': 'update' });
                                }
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TicketsPage.prototype.backTab = function () {
        //this.router.navigate(['tabs/tab2']);  
        this.modalCtrl.dismiss();
    };
    TicketsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tickets',
            template: __webpack_require__(/*! ./tickets.page.html */ "./src/app/pago/tickets/tickets.page.html"),
            styles: [__webpack_require__(/*! ./tickets.page.scss */ "./src/app/pago/tickets/tickets.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], src_app_services_tickets_service__WEBPACK_IMPORTED_MODULE_3__["TicketsService"], src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__["GeneralService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], TicketsPage);
    return TicketsPage;
}());



/***/ }),

/***/ "./src/app/services/tickets.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/tickets.service.ts ***!
  \*********************************************/
/*! exports provided: TicketsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketsService", function() { return TicketsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _constant_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constant.service */ "./src/app/services/constant.service.ts");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");





var TicketsService = /** @class */ (function () {
    function TicketsService(http, constant, httpNative) {
        this.http = http;
        this.constant = constant;
        this.httpNative = httpNative;
        this.headers = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
        this.api_url = this.constant.apiConnect;
    }
    TicketsService.prototype.createHeader = function (token) {
        this.headers = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded', 'token': token }) };
    };
    TicketsService.prototype.searchTicket = function (params, token) {
        this.createHeader(token);
        var url = this.api_url + 'tickets/buscar-code';
        return this.http.post(url, params, this.headers);
    };
    TicketsService.prototype.payTicket = function (params, userId, ticketId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/ticket/' + ticketId + '/pay';
        return this.http.post(url, params, this.headers);
    };
    TicketsService.prototype.paymentsTickets = function (userId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/tickets/payment';
        return this.http.get(url, this.headers);
    };
    TicketsService.prototype.getTicket = function (userId, ticketId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/ticket/' + ticketId;
        return this.http.get(url, this.headers);
    };
    TicketsService.prototype.sendToInvoice = function (userId, ticketId, params, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/invoice/ticket/' + ticketId;
        return this.http.post(url, params, this.headers);
    };
    TicketsService.prototype.getListInvoices = function (userId, token) {
        this.createHeader(token);
        var url = this.api_url + 'user/' + userId + '/invoices';
        return this.http.get(url, this.headers);
    };
    TicketsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _constant_service__WEBPACK_IMPORTED_MODULE_3__["ConstantService"], _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__["HTTP"]])
    ], TicketsService);
    return TicketsService;
}());



/***/ })

}]);
//# sourceMappingURL=pago-pago-module.js.map