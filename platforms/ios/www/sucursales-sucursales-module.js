(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sucursales-sucursales-module"],{

/***/ "./src/app/sucursales/sucursales.module.ts":
/*!*************************************************!*\
  !*** ./src/app/sucursales/sucursales.module.ts ***!
  \*************************************************/
/*! exports provided: SucursalesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SucursalesPageModule", function() { return SucursalesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sucursales_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sucursales.page */ "./src/app/sucursales/sucursales.page.ts");







var routes = [
    {
        path: '',
        component: _sucursales_page__WEBPACK_IMPORTED_MODULE_6__["SucursalesPage"]
    }
];
var SucursalesPageModule = /** @class */ (function () {
    function SucursalesPageModule() {
    }
    SucursalesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_sucursales_page__WEBPACK_IMPORTED_MODULE_6__["SucursalesPage"]]
        })
    ], SucursalesPageModule);
    return SucursalesPageModule;
}());



/***/ }),

/***/ "./src/app/sucursales/sucursales.page.html":
/*!*************************************************!*\
  !*** ./src/app/sucursales/sucursales.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>  \n    <ion-toolbar color=\"baseApp\">\n        <ion-buttons slot=\"start\">\n        <ion-menu-button></ion-menu-button>\n        </ion-buttons>\n        <ion-title  >Sucursales  </ion-title>        \n    </ion-toolbar>\n</ion-header> \n\n<ion-content>\n\n    <div id=\"map\" style=\"width:100%; height: 100%;\"></div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/sucursales/sucursales.page.scss":
/*!*************************************************!*\
  !*** ./src/app/sucursales/sucursales.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1Y3Vyc2FsZXMvc3VjdXJzYWxlcy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/sucursales/sucursales.page.ts":
/*!***********************************************!*\
  !*** ./src/app/sucursales/sucursales.page.ts ***!
  \***********************************************/
/*! exports provided: SucursalesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SucursalesPage", function() { return SucursalesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");



var SucursalesPage = /** @class */ (function () {
    function SucursalesPage(geolocation) {
        this.geolocation = geolocation;
    }
    SucursalesPage.prototype.ngOnInit = function () {
        this.getPosition();
    };
    SucursalesPage.prototype.getPosition = function () {
        var _this = this;
        var mapEle = document.getElementById('map');
        this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
            console.log('*** cordenadas ***', resp.coords);
            _this.lat = resp.coords.latitude;
            _this.lng = resp.coords.longitude;
            setTimeout(function () {
                _this.invokeMap();
            }, 300);
        }).catch(function (error) {
        });
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
        });
    };
    SucursalesPage.prototype.invokeMap = function () {
        var mapEle = document.getElementById('map');
        var myPos = {
            lat: this.lat,
            lng: this.lng
        };
        var map = new google.maps.Map(mapEle, {
            center: myPos,
            zoom: 12
        });
    };
    SucursalesPage.prototype.sayHi = function () {
        this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
            console.log('*** cordenadas ***', resp.coords);
        }).catch(function (error) {
            console.log('Error getting location', error);
        });
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            console.log('*** if an error occurred ***', data);
            // data can be a set of coordinates, or an error (if an error occurred).
            // data.coords.latitude
            // data.coords.longitude
        });
    };
    SucursalesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sucursales',
            template: __webpack_require__(/*! ./sucursales.page.html */ "./src/app/sucursales/sucursales.page.html"),
            styles: [__webpack_require__(/*! ./sucursales.page.scss */ "./src/app/sucursales/sucursales.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_2__["Geolocation"]])
    ], SucursalesPage);
    return SucursalesPage;
}());



/***/ })

}]);
//# sourceMappingURL=sucursales-sucursales-module.js.map