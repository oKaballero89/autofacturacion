(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~session-login-login-module~session-session-module"],{

/***/ "./src/app/services/constant.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/constant.service.ts ***!
  \**********************************************/
/*! exports provided: ConstantService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConstantService", function() { return ConstantService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConstantService = /** @class */ (function () {
    function ConstantService() {
        this.apiConnect = 'http://192.168.8.163/proveedores-api/web/app_dev.php/api/v1/';
    }
    ConstantService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConstantService);
    return ConstantService;
}());



/***/ }),

/***/ "./src/app/services/general.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/general.service.ts ***!
  \*********************************************/
/*! exports provided: GeneralService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneralService", function() { return GeneralService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _constant_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./constant.service */ "./src/app/services/constant.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var GeneralService = /** @class */ (function () {
    function GeneralService(toast, constant) {
        this.toast = toast;
        this.constant = constant;
        this.api_url = this.constant.apiConnect;
    }
    GeneralService.prototype.msgAlert = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toast.create({
                            message: message,
                            duration: 2000,
                            position: 'top'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    GeneralService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _constant_service__WEBPACK_IMPORTED_MODULE_2__["ConstantService"]])
    ], GeneralService);
    return GeneralService;
}());



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _constant_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constant.service */ "./src/app/services/constant.service.ts");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");





var UserService = /** @class */ (function () {
    function UserService(http, constant, httpNative) {
        this.http = http;
        this.constant = constant;
        this.httpNative = httpNative;
        this.headers = { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
        this.api_url = this.constant.apiConnect;
    }
    UserService.prototype.createHeader = function (token) {
    };
    UserService.prototype.authUser = function (params) {
        console.log("::  parametros para enviasr ::", params);
        return this.http.post(this.api_url + 'user/login', params, this.headers);
    };
    UserService.prototype.restorePassword = function (email) {
        var url = this.api_url + 'user/restore_password';
        return this.http.post(url, { 'email': email }, this.headers);
    };
    UserService.prototype.registerUser = function (params) {
        var url = this.api_url + 'user/register';
        return this.http.post(url, params, this.headers);
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _constant_service__WEBPACK_IMPORTED_MODULE_3__["ConstantService"], _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_4__["HTTP"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/services/validator.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/validator.service.ts ***!
  \***********************************************/
/*! exports provided: ValidatorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidatorService", function() { return ValidatorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ValidatorService = /** @class */ (function () {
    function ValidatorService() {
    }
    ValidatorService.prototype.validationLogin = function (inputs) {
        console.log(inputs);
        if (inputs.username == '' || inputs.username == undefined) {
            return { success: false, msg: 'Debes ingresar usuario' };
        }
        if (inputs.password == '' || inputs.password == undefined) {
            return { success: false, msg: 'Debes ingresar contraseña' };
        }
        return { success: true, msg: 'todo bien' };
    };
    ValidatorService.prototype.validationRegister = function (inputs) {
        console.log(' ::: inputs :::', inputs);
        if (inputs.name == undefined || inputs.name == '') {
            return { success: false, msg: 'El nombre es requerido' };
        }
        if (inputs.last_name == undefined || inputs.last_name == '') {
            return { success: false, msg: 'Los apellidos son requeridos' };
        }
        if (inputs.email == undefined || inputs.email == '') {
            return { success: false, msg: 'El email es requerido' };
        }
        if (!this.validateEmail(inputs.email)) {
            return { success: false, msg: 'Debes ingresar un email válido' };
        }
        if (inputs.password == undefined || inputs.password == '') {
            return { success: false, msg: 'La contraseña es requerida' };
        }
        if (inputs.cpassword == undefined || inputs.cpassword == '') {
            return { success: false, msg: 'Debes confirmar contraseña' };
        }
        if (inputs.password != inputs.cpassword) {
            return { success: false, msg: 'Las contraseñas no coinciden' };
        }
        return { success: true, msg: '' };
    };
    ValidatorService.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    ValidatorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ValidatorService);
    return ValidatorService;
}());



/***/ }),

/***/ "./src/app/session/register/register.page.html":
/*!*****************************************************!*\
  !*** ./src/app/session/register/register.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>login</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <form class=\"example-form\">\n    <mat-form-field class=\"example-full-width\">\n      <input matInput placeholder=\"Favorite food\" value=\"Sushi\">\n    </mat-form-field>\n  \n  </form>\n</ion-content> -->\n<ion-content style=\"background-image: url('assets/imgs/system/Inicio-2732x4857.jpg');\" text-center>\n\n  <div class=\"conta\">\n    <img class=\"logo\" src=\"https://es.freelogodesign.org/Content/img/logo-ex-7.png\">\n\n    <div class=\"title\">Iniciar sesión</div>\n\n\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Nombre\" [(ngModel)]=\"register.name\">\n          </mat-form-field>\n       </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Apellidos\" [(ngModel)]=\"register.last_name\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n            <input matInput placeholder=\"Correo\" [(ngModel)]=\"register.email\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n              <input matInput placeholder=\"Contraseña\" [(ngModel)]=\"register.password\" type=\"password\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n          <mat-form-field class=\"example-full-width\" style=\"width:100%\">\n              <input matInput placeholder=\"Confirmar contraseña\" [(ngModel)]=\"register.cpassword\" type=\"password\">\n          </mat-form-field>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col>\n            <ion-button expand=\"block\" (click)=\"registerUser()\" >Registrar</ion-button>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n  </div>\n\n</ion-content>\n\n\n<ion-footer class=\"footerButton\" [routerLink]=\"['/']\">\n  ¿Ya tienes cuenta? Inicia sesión\n</ion-footer>"

/***/ }),

/***/ "./src/app/session/register/register.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/session/register/register.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Nlc3Npb24vcmVnaXN0ZXIvcmVnaXN0ZXIucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/session/register/register.page.ts":
/*!***************************************************!*\
  !*** ./src/app/session/register/register.page.ts ***!
  \***************************************************/
/*! exports provided: RegisterPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPage", function() { return RegisterPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/validator.service */ "./src/app/services/validator.service.ts");
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/user.service */ "./src/app/services/user.service.ts");
/* harmony import */ var src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/general.service */ "./src/app/services/general.service.ts");





var RegisterPage = /** @class */ (function () {
    function RegisterPage(validationService, userService, generalService) {
        this.validationService = validationService;
        this.userService = userService;
        this.generalService = generalService;
        this.register = {};
    }
    RegisterPage.prototype.ngOnInit = function () {
    };
    RegisterPage.prototype.registerUser = function () {
        var _this = this;
        var validation = this.validationService.validationRegister(this.register);
        if (validation.success) {
            this.userService.registerUser(this.register).subscribe(function (res) {
                if (res.code != 200) {
                    _this.generalService.msgAlert(res.msg);
                    return;
                }
                var msg = 'Usuario creado correctamente, en breve recibirás un correo para activar tu cuenta';
                _this.generalService.msgAlert(msg);
                _this.register = {};
            });
        }
        else {
            this.generalService.msgAlert(validation.msg);
            return;
        }
    };
    RegisterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.page.html */ "./src/app/session/register/register.page.html"),
            styles: [__webpack_require__(/*! ./register.page.scss */ "./src/app/session/register/register.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_validator_service__WEBPACK_IMPORTED_MODULE_2__["ValidatorService"], src_app_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], src_app_services_general_service__WEBPACK_IMPORTED_MODULE_4__["GeneralService"]])
    ], RegisterPage);
    return RegisterPage;
}());



/***/ })

}]);
//# sourceMappingURL=default~session-login-login-module~session-session-module.js.map