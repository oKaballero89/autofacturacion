import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
declare var google;

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.page.html',
  styleUrls: ['./sucursales.page.scss'],
})
export class SucursalesPage implements OnInit {
  
  constructor(private geolocation: Geolocation) {}

  ngOnInit() {
    this.getPosition();
  }
  lat = 19.427132;
  lng = -99.167661;
  getPosition(){
    
    const mapEle:HTMLElement = document.getElementById('map');

    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      console.log('*** cordenadas ***',resp.coords);
      // this.lat= resp.coords.latitude;
      // this.lng = resp.coords.longitude;
      setTimeout(() => {
        this.invokeMap();  
      }, 300);
        
      
     }).catch((error) => {
     });
     
     let watch = this.geolocation.watchPosition();
     watch.subscribe((data) => {
     });
  }


  invokeMap(){
    const mapEle:HTMLElement = document.getElementById('map');
    let myPos = {
      lat: this.lat,
      lng: this.lng
    };

    const map = new google.maps.Map(mapEle, {
      center: myPos,
      zoom: 12
    })
  }

  sayHi(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      console.log('*** cordenadas ***',resp.coords);
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     
     let watch = this.geolocation.watchPosition();
     watch.subscribe((data) => {
      console.log('*** if an error occurred ***', data);
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
     });
  }
}
