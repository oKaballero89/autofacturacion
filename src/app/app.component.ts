import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { GeneralService } from './services/general.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public appPages = [
    {
      title: 'Mi información',
      keyW: 'info',
      url: '/profile/detail',
      icon: 'home',
      type: 'link'
    },
    // {
    //   title: 'Cambiar contraseña',
    //   keyW: 'chPw',
    //   url: '/profile/changePassword',
    //   icon: 'lock',
    //   type: 'link'
    // },

    {
      title: 'RFCs',
      keyW: 'chPw',
      url: '/profile/rfc/list',
      icon: 'document',
      type: 'link'
    },

    {
      title: 'Mis tarjetas',
      keyW: 'chPw',
      url: '/profile/card/list',
      icon: 'card',
      type: 'link'
    },

    
    {
      title: 'Sucursales',
      keyW: 'suc',
      url: '/sucursales',
      icon: 'pin',
      type: 'link'
    },
    {
      title: 'Pagos',
      keyW: 'pays',
      url: '/pago',
      icon: 'wallet',
      type: 'link'
    },
    {
      title: 'Promociones',
      keyW: 'prom',
      url: '/promocion',
      icon: 'gift',
      type: 'link'
    },
    {
      title: 'Califica la app',
      keyW: 'qualification',
      url: '',
      icon: 'star-outline',
      type: 'function'
    },
    {
      title: 'Recomiendanos',
      keyW: 'recom',
      url: '',
      icon: 'share-alt',
      type: 'function'
    },
    {
      title: 'Cerrar sesión',
      keyW: 'close',
      url: '',
      icon: 'power',
      type: 'function'
    }

  ];
  
  sessionActive = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public generalService: GeneralService
  ) {
    this.initializeApp();

    let session = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));

    if(session){ this.sessionActive = true }

    this.generalService.listenerSession.subscribe((res:any) =>{
      this.sessionActive = res;
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  close(){
    this.generalService.outSession();
    this.sessionActive = false;
  }
}
