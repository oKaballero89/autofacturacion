import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InvoicesPage } from './invoices.page';
import { FacturaExpressComponent } from './factura-express/factura-express.component';
import { MatExpansionModule, MatIconModule, MatCardModule, MatInputModule,MatButtonModule, MatListModule, MatProgressBarModule, MatMenuModule,MatRadioModule,MatToolbarModule , MatSelectModule, MatSidenavModule } from '@angular/material';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';

const routes: Routes = [
  {
    path: '',
    component: InvoicesPage
  }, {
    path: 'factura-express',
    component: FacturaExpressComponent
  },{
    path: 'ticketDetail',
    component: TicketDetailComponent
  }

  //  {
  //   path: 'modalRfc', 
  //   component: RfcComponent
  // }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,        
    MatInputModule,
    MatRadioModule,
    MatToolbarModule ,
    MatSelectModule,
    MatSidenavModule,
    MatExpansionModule
  ],
  declarations: [InvoicesPage, FacturaExpressComponent, TicketDetailComponent ]
})
export class InvoicesPageModule {}
