import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss'],
})
export class TicketDetailComponent implements OnInit {

  info;
  rows:any = [];
  constructor(public params: NavParams, public modalCtrl: ModalController) {
    this.info = this.params.get('msg');
    console.log(':::--:::', this.info);
    this.rows =  this.info.conceptos;
   }

  ngOnInit() {}

  backTab(){
    this.modalCtrl.dismiss();
  }

}
