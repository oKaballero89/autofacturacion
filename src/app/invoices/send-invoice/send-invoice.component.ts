import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ModalController, IonSlides, NavParams, LoadingController } from '@ionic/angular';
import { GeneralService } from 'src/app/services/general.service';
import { TicketsService } from 'src/app/services/tickets.service';
import { RfcComponent } from 'src/app/profile/rfc/rfc.component';
import { AddRfcComponent } from 'src/app/pago/add-rfc/add-rfc.component';

@Component({
  selector: 'app-send-invoice',
  templateUrl: './send-invoice.component.html',
  styleUrls: ['./send-invoice.component.scss'],
})
export class SendInvoiceComponent implements OnInit {

  userId;
  userToken;
  rows:any = [];
  code;
  company;
  ticket;
  rfcSelected;
  rfcUser;


  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  constructor(public userService: UserService, public modalCtrl: ModalController, public generalService: GeneralService, public ticketService: TicketsService, public navParams: NavParams,
    public loading: LoadingController) { 
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];
    this.company = b.company;
    this.code = this.navParams.get('ticket');    
      console.log('debe facturar')
    setTimeout(() => {
      this.getRfcs();
      this.getTicketInfo();
    }, 500);
  }

  getRfcs(){
    this.userService.listRfcs(this.userId, this.userToken).subscribe((res:any) => {      
      //this.rfcSelected = res.msg[0].id;
      this.rows = res.msg;
      console.log(this.rows);
    });
  }

  addressData(data){
    if(data.city){
      return data.street+' '+ data.num_ext + ' int.'+data.num_int+' '+data.colony+ ' CP '+data.postal_code+ ' '+data.city+' '+data.state;
    }else{
      return "Sin dirección ";
    }
  }

  getTicketInfo(){
    let params = { 
      "companyId" : this.company.id,
      "noTicket": this.code
    }
    this.ticketService.searchTicket(params, this.userToken).subscribe((res:any)=>{         
      if(res.code == 501){
        this.generalService.msgAlert(res.msg);
        this.generalService.outSession();
        return;
      }else if(res.code == 200){
        this.ticket = res.msg;    
      }
    });
  }


  ngOnInit() {}

  backTab(){
    this.modalCtrl.dismiss();
  }

  
  async sendInvoice(code) {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: SendInvoiceComponent,
          
          componentProps: {'ticket': code}
    });
     
    modal.onDidDismiss().then((res) => {
      console.log(':: cerro modal', res);
      if(res){
        // Pregunta si quiere facturar //
        
      }
    });
    
    await modal.present();
  }

  sendToInvoce(){
    if(!this.rfcSelected){
      this.generalService.msgAlert('Debes seleccionar RFC de facturación');
      return;
    }
    this.generalService.loading('Facturando compra')
    let params = {rfcId: this.rfcUser };
    
    this.ticketService.sendToInvoice(this.userId, this.ticket.id, params,this.userToken).subscribe((res:any) => {
      this.loading.dismiss();
      if(res.code == 200){
        this.generalService.msgAlert('Tu factura ha sido generada correctamente');
        setTimeout(() => {
          this.modalCtrl.dismiss({'status': 'update'});  
        }, 500);
      }
    }, err => {
      this.loading.dismiss();
      this.generalService.msgAlert('No se pudo facturar en este momento, intentalo más tarde');
    });

  }
  
  showValue(valor){
    this.rfcUser = valor.id
    console.log(valor);
  }

  async addRfc() {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: AddRfcComponent,          
          componentProps: {}
    });
     
    modal.onDidDismiss().then((res) => {
      if(res.data){
        // Pregunta si quiere facturar //
        this.getRfcs();
      }
    });
    
    await modal.present();
  }
}
