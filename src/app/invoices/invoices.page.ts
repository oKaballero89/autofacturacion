import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from '../services/general.service';
import { TicketsService } from '../services/tickets.service';
import { LoadingController, ModalController } from '@ionic/angular';
import { ConstantService } from '../services/constant.service';
import { FacturaExpressComponent } from './factura-express/factura-express.component';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.page.html',
  styleUrls: ['./invoices.page.scss'],
})
export class InvoicesPage implements OnInit {

  isOn = false;
  scannedData: {};
  qrScan:any;
  inputTicket;
  user;
  user_token;
  company;
  codeInput;
  rows:any = [];
  sucursal;

  constructor(public conts: ConstantService, public router: Router, public generalService: GeneralService, public ticketService: TicketsService, public loading: LoadingController, 
    public modalCtrl: ModalController, public qrScanner: QRScanner) { 
    this.company = this.conts.companyApp;
    this.inputTicket = 'Bn30ZP';

    setTimeout(() => {
      this.getOffices();
    }, 300);
  }

  ngOnInit() {
  }

  backTab(){
    this.router.navigate(['']);  
  }


  activeSearch(code){
    if(!this.sucursal){
      this.generalService.msgAlert('Debes seleccionar sucursal');
      return;
    }
    this.codeInput = code;
    this.generalService.loading('Buscando ticket');
    let params = { 
      "companyId" : this.company,
      "sucursal": this.sucursal,
      "noTicket": code
    }

    this.ticketService.searchTicketExpress(params, this.user_token).subscribe((res:any)=>{
      this.loading.dismiss();
      
      if(res.code == 501){
        this.generalService.msgAlert(res.msg);
      }else if(res.code == 200){        
        setTimeout(()=>{
          this.openModal(res.msg);
        }, 400);
      }

    });
  }

  async openModal(params) {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: FacturaExpressComponent,          
          componentProps: {'msg': params, sucursal: this.sucursal}
    });
     
    modal.onDidDismiss().then((res) => {
      console.log(':: cerro modal', res);
      if(res.data.status == 'invoice'){
        // Pregunta si quiere facturar //
        // this.sendInvoice(this.inputTicket);
      }
      else if(res.data.status == 'update'){
        this.backTab();
      }
    });
    
    await modal.present();
  }

  countSucursales = 0;

  getOffices(){    
    this.ticketService.sucursalesCompany(this.conts.companyApp).subscribe((res:any)=>{      
      this.countSucursales = res.msg.length;
      this.rows = res.msg;

      if(this.countSucursales == 1){
        this.sucursal = res.msg[0].id;
      }
      
    }, err=>{
      console.log(' hay errororororororro');
    });

  }

  scann(){  
    // Optionally request the permission early
    this.qrScanner.prepare().then((status: QRScannerStatus) => {
      if(status.authorized){
        this.qrScanner.show();
        document.getElementsByTagName("body")[0].style.opacity = "0";        
        this.qrScan = this.qrScanner.scan().subscribe((textFound) => {
          document.getElementsByTagName("body")[0].style.opacity = "1";
          this.qrScan.unsubscribe();
          // console.info(textFound);
          // console.log('::: debio scanear :::');
          this.activeSearch(textFound);
          setTimeout(() => {
            
          }, 20);  

        }, err => {
          console.log(JSON.stringify(err));
        })
      }else if(status.denied){        
      }
    });          
  }


}
