import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { GeneralService } from 'src/app/services/general.service';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { CardComponent } from '../card/card.component';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss'],
})
export class CardsComponent implements OnInit {

  userId;
  userToken;
  clientId;
  cards:any = [];
  load = false;
  constructor(public userServices: UserService, public generalService: GeneralService, public loading: LoadingController, public alertController: AlertController,
    public modalCtrl: ModalController) { 
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];
    this.clientId = b['client'];
    this.generalService.loading('Cargando');
    setTimeout(() => {
      this.getList();
    }, 800);
  }

  ngOnInit() {}

  getList(){    
    this.userServices.listCards(this.userId, this.userToken).subscribe((res:any)=>{
      this.loading.dismiss();
      this.load = true;
      this.cards = res.msg;
    }) ;
  }

  deleteCard(card){
    this.generalService.loading('Eliminando tarjeta');
    this.userServices.deleteCard(this.userId, card.id, this.userToken).subscribe((res:any) => {
      this.loading.dismiss();
      if(res.code==200){
        this.generalService.msgAlert('Tarjeta eliminada correctamente');
      }else{

      }
    }, err =>{
      this.loading.dismiss(),
      this.generalService.msgAlert('No se pudo eliminar tarjeta');
    });
  }

  async confirmDelete(row) {
    const alert = await this.alertController.create({
      header: 'Eliminar tarjeta',
      message: '¿Realmente deseas eliminar esta tarjeta?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.deleteCard(row);
          }
        }
      ]
    });

    await alert.present();
  }

  async addCard() {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: CardComponent,
          
          componentProps: {}
    });
     
    modal.onDidDismiss().then((res) => {
      if(res.data){
        // Pregunta si quiere facturar //
        this.getList();
      }
    });
    
    await modal.present();
  }

}
