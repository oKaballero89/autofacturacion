import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-rfc',
  templateUrl: './rfc.component.html',
  styleUrls: ['./rfc.component.scss'],
})
export class RfcComponent implements OnInit {
  title = 'Nuevo';
  form={};
  invoiceData:any = {};
  userId;
  userToken;
  company;

  constructor(public generalService: GeneralService, public userService: UserService, public router: Router,
    public modalCtrl: ModalController) { 
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];
  }

  ngOnInit() {}

  addRfc(){
    if(!this.invoiceData.razon_social || this.invoiceData.razon_social == undefined ){
      this.generalService.msgAlert('Debes ingresar la razón social')
      return ;
    }

    if(!this.invoiceData.RFC || this.invoiceData.RFC == undefined ){
      this.generalService.msgAlert('Debes ingresar RFC ')
      return ;
    }

    this.userService.addRfc(this.invoiceData, this.userId, this.userToken).subscribe((res:any) => {
      if(res.code == 200){
        this.generalService.msgAlert('RFC agregado correctamente');
        setTimeout(() => {
          this.router.navigateByUrl('/profile/rfc/list');
          this.invoiceData = {};
        }, 800);
      }else{
        this.generalService.msgAlert(res.msg);
      }
      console.log(res);
    } );

 } 

  backTab(){
    this.modalCtrl.dismiss()
  }
}
