import { Component, OnInit } from '@angular/core';
import { ConstantService } from 'src/app/services/constant.service';
import { ToastController, ModalController, LoadingController } from '@ionic/angular';
import { GeneralService } from 'src/app/services/general.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
declare var Conekta:any

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {

  title = 'Agregar';
  userId;
  userToken;
  clientId;
  
  cardInfo:any = {
    cardNumber: null,
    cardName: null,
    cardVigency: null,
    cardCvv: null,
    expiryMonth:null,
    expiryYear:null
  }
  
  constructor(public conf: ConstantService, public generalService: GeneralService, public userService: UserService, public router: Router,
    public modalCtrl: ModalController, public loading: LoadingController) { 
    Conekta.setPublicKey(this.conf.conektaKey);
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];
    this.clientId = b['client'];

  }

  backTab(){
    this.modalCtrl.dismiss();
  }
  ngOnInit() {}

  saveData(){
    //return;
    this.generalService.loading('Agregando tarjeta');
    this.validate(res=>{
      
      if(!res.success){
        this.generalService.msgAlert(res.msg);
        return false;
      }

      let brand = Conekta.card.getBrand(this.cardInfo.cardNumber);
      this.cardInfo.provider = brand;

      let tokenParams = {
        "card": {
          "number": this.cardInfo.cardNumber,
          "name": this.cardInfo.cardName,
          "exp_month": this.cardInfo.expiryMonth,
          "exp_year": this.cardInfo.expiryYear,
          "cvc": this.cardInfo.cardCvv
        }
      };

      Conekta.token.create(tokenParams, (token) => {
        
      this.cardInfo.tokenId = token.id; // token generado por conekta

      console.log(this.cardInfo);
        let params = {
          cardData : this.cardInfo
        }

        this.userService.addCard(this.userId, params, this.userToken).subscribe((res:any) => {
          this.loading.dismiss();
          if(res.code == 200){
            this.generalService.msgAlert('Tarjeta agregada correctamente');
            this.modalCtrl.dismiss({'status' : 'update'});
            setTimeout(() => {
                this.router.navigate(['profile/card/list']);
            }, 600);
          }else if(res.code == 500){
            this.generalService.msgAlert(res.message);
          }
        })

      }, error => {
        console.log(error);
      });
    });

  }

  validate( call){

    if(!this.cardInfo.cardNumber){
      return call({success : false, msg: "Debe ingresar número de la tarjeta"});
    }

    if(!this.cardInfo.cardName){
      return call({success : false, msg: "Debe ingresar nombre de tarjetahabiente de la tarjeta"});
    }

    if(!this.cardInfo.expirate ){
      return call({success : false, msg: "Debe ingresar la vigencia de la tarjeta"});
    }

    if(!this.cardInfo.cardCvv){
      return call({success : false, msg: "Debe ingresar CVV de la tarjeta"});
    }

    if(!Conekta.card.validateNumber(this.cardInfo.cardNumber)){
      return call({success: false, msg: "El número de tarjeta es incorrecto"});
    }

    // if(!this.invoiceData.razonSocial){
    //   return call({success: false, msg: "Debe ingresar razón social de la empresa"});
    // }

    // if(!this.invoiceData.rfc){
    //   return call({success: false, msg: "Debe ingresar RFC de la empresa"});
    // }

    let month = this.cardInfo.expirate.slice(0,2);
    let year = this.cardInfo.expirate.slice(-2);

    this.cardInfo.cardVigency= month+'/'+year;
    this.cardInfo.expiryMonth= month;
    this.cardInfo.expiryYear = year;

    if(!Conekta.card.validateExpirationDate(this.cardInfo.expiryMonth, this.cardInfo.expiryYear) ){
      return call({success: false, msg: "La fecha de expiración es inválida"});
    }

    return call({success : true, msg: "Todo bien"});

  }

}
