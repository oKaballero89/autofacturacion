import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { GeneralService } from 'src/app/services/general.service';
import { LoadingController, ModalController, AlertController } from '@ionic/angular';
import { RfcComponent } from '../rfc/rfc.component';

@Component({
  selector: 'app-rfcs',
  templateUrl: './rfcs.component.html',
  styleUrls: ['./rfcs.component.scss'],
})
export class RfcsComponent implements OnInit {

  userId;
  userToken;
  company;
  rows:any = [];
  load = false;

  constructor(public userService: UserService, public generalService: GeneralService, public loading: LoadingController,
    public modalCtrl: ModalController, public alertController: AlertController) { 
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];
    
    this.generalService.loading('Cargando');
    setTimeout(() => {
      this.getRfcs();
    }, 500);
  }

  ngOnInit() {}

  getRfcs(){
    this.userService.listRfcs(this.userId, this.userToken).subscribe((res:any) => {
      this.rows = res.msg;
      this.load =  true;
      this.loading.dismiss();
      this.load = true;
    }, err=>{
      this.loading.dismiss();
      if(err.error.title == 'Token inválido'){
        this.generalService.outSession();
      }
    });
  }

  

  async addRfc() {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: RfcComponent,          
          componentProps: {}
    });
     
    modal.onDidDismiss().then((res) => {
      if(res.data){
        // Pregunta si quiere facturar //
        this.getRfcs();
      }
    });
    
    await modal.present();
  }

  async editRfc(data) {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: RfcComponent,          
          componentProps: {msg: data}
    });
     
    modal.onDidDismiss().then((res) => {
      if(res.data){
        // Pregunta si quiere facturar //
        this.getRfcs();
      }
    });
    
    await modal.present();
  }

  deleteRfc(rfc){
    
    this.generalService.loading('Eliminando RFC');
    this.userService.deleteRFC(this.userId, rfc.id, this.userToken).subscribe((res:any) => {
      this.loading.dismiss();
      if(res.code==200){
        this.generalService.msgAlert('RFC eliminado correctamente');
        this.getRfcs();
      }else{

      }
    }, err =>{
      this.loading.dismiss(),
      this.generalService.msgAlert('No se pudo eliminar el RFC');
    });
  }

  async confirmDelete(row) {
    
    const alert = await this.alertController.create({
      header: 'Eliminar RFC',
      message: '¿Realmente deseas eliminar este RFC?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.deleteRfc(row);
          }
        }
      ]
    });

    await alert.present();
  }

}
