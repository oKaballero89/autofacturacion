import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePage } from './profile.page';
import { DetailProfileComponent } from './detail-profile/detail-profile.component';
import { MatExpansionModule, MatIconModule, MatCardModule, MatInputModule,MatButtonModule, MatListModule, MatProgressBarModule, MatMenuModule,MatRadioModule,MatToolbarModule , MatSelectModule, MatSidenavModule } from '@angular/material';
import { CardsComponent } from './cards/cards.component';
import { CardComponent } from './card/card.component';
import { RfcsComponent } from './rfcs/rfcs.component';
import { RfcComponent } from './rfc/rfc.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import {NgxMaskModule} from 'ngx-mask'

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  },
  {
    path: 'detail',
    component: DetailProfileComponent
  },{
    path: 'card',
    children:  [
        {
          path: 'list',
          component: CardsComponent
        },{
          path: 'new',
          component: CardComponent
        },
        {
          path: 'edit/:id',
          component: CardComponent
        }
    ]
  },{
    path: 'rfc',
    children:[
      {
        path: 'list',
        component: RfcsComponent
      },{
        path: 'new',
        component: RfcComponent
      }
    ]
  },
  {
    path: 'changePassword',
    component: ChangePasswordComponent
  }

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,        
    MatInputModule,
    MatRadioModule,
    MatToolbarModule ,
    MatSelectModule,
    MatSidenavModule,
    MatExpansionModule,
    FormsModule,
    NgxMaskModule.forRoot()  
  ],
  declarations: [ProfilePage, DetailProfileComponent,CardsComponent, CardComponent, RfcsComponent, RfcComponent, ChangePasswordComponent]
})
export class ProfilePageModule {}
