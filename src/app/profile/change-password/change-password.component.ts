import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {

  formUsr:any={};
  userId;
  userToken;
  company;

  constructor(public router: Router, public userService: UserService, public generalService: GeneralService) { 
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];

  }

  ngOnInit() {}

  updatePassword(){
    // if(!this.formUsr.oldPassword || this.formUsr.oldPassword == undefined ){
    //   this.generalService.msgAlert('Debes ingresar tu actual contraseña');
    //   return;
    // }

    if(!this.formUsr.password || this.formUsr.password == undefined ){
      this.generalService.msgAlert('Debes ingresar tu nueva contraseña');
      return;
    }

    if(!this.formUsr.cPassword || this.formUsr.cPassword == undefined ){
      this.generalService.msgAlert('Debes confirmar tu nueva contraseña');
      return;
    }

    if(this.formUsr.cPassword != this.formUsr.password  ){
      this.generalService.msgAlert('La nueva contraseña no coincide');
      return;
    }

    this.userService.changePassword(this.userId, {password: this.formUsr.password} ,this.userToken ).subscribe((res:any)=>{
      console.log(res);
      if(res.success){
        this.formUsr ={};
        this.generalService.msgAlert('Contraseña actualizada correctamente');
      }
    });


  }

}
