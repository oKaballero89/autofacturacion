import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
  }

  buttonClick(){
    
  }

  closeSession(){
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
