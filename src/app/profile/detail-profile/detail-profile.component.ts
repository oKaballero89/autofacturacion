import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router, LoadChildren } from '@angular/router';
import { GeneralService } from 'src/app/services/general.service';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-detail-profile',
  templateUrl: './detail-profile.component.html',
  styleUrls: ['./detail-profile.component.scss'],
})
export class DetailProfileComponent implements OnInit {

  user;
  photo = 'assets/imgs/default_profile.jpg';
  profile = {};
  load = false;
  section = 'gral';
  formUsr:any={};
  
  constructor(public userService: UserService, public router: Router, public loading: LoadingController, public generalService: GeneralService) { 
    this.user = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    setTimeout(() => {
      this.getInfo();
    }, 300);
  }

  ngOnInit() {}

  segmentChanged(){
    console.log(this.section)
    if(this.section == 'gral'){
      this.section = 'password';
    }else{
      this.section = 'gral';
    }
  }

  getInfo(){
    this.generalService.loading('cargando');

    this.userService.userInfo(this.user.id, this.user.session_token).subscribe((res:any)=>{
      this.loading.dismiss();
      this.load = true;
      if(res.code == 401){
        localStorage.clear();
        this.router.navigate(['/']);  
        return;
      }

      this.profile = {
        name: res.msg.name,
        lastName: res.msg.last_name,
        email: res.msg.email
      }
      
    }, error=>{
      if(error.status == 401){
        localStorage.clear();
        this.router.navigate(['/']);  
      }
    });
  }
}
