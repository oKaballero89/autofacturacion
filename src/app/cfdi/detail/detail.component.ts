import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {

  img: any;
  sliderOpts = {
    zoom: {
      maxRatio: 5
    }
  };

  constructor(public navParams: NavParams, public modalController: ModalController) { 
    this.img = this.navParams.get('pathUrl');    
  }

  ngOnInit() {}

  
  close(){
    this.modalController.dismiss();
  }
    

}
