import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PromotionsPage } from './promotions.page';
import { MatExpansionModule, MatIconModule, MatCardModule, MatInputModule,MatButtonModule, MatListModule, MatProgressBarModule, MatMenuModule,MatRadioModule,MatToolbarModule , MatSelectModule, MatSidenavModule } from '@angular/material';


const routes: Routes = [
  {
    path: '',
    component: PromotionsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,        
    MatInputModule,
    MatRadioModule,
    MatToolbarModule ,
    MatSelectModule,
    MatSidenavModule,
    MatExpansionModule
  ],
  declarations: [PromotionsPage]
})
export class PromotionsPageModule {}
