import { Component, OnInit } from '@angular/core';
import { TicketsService } from '../services/tickets.service';
import { throwToolbarMixedModesError } from '@angular/material';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.page.html',
  styleUrls: ['./promotions.page.scss'],
})
export class PromotionsPage implements OnInit {

  promotions:any = [];
  userId;
  companyId;
  userToken;
  company;
  

  constructor(public ticketService: TicketsService) { 
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];
    this.company = b.company;
    setTimeout(() => {
      this.getPromos();
    }, 300);
  }

  ngOnInit() {
  }

  getPromos(){
    this.ticketService.promociones(this.userId, this.company.id, this.userToken).subscribe((res:any)=>{
      console.log( res.msg);
      this.promotions = res.msg;
      //this.promotions.push(res.msg);
      //console.log( this.promotions[0].imgs_promo[0].image  );
    });
  }

  imgPromo(rowUrl){
    let linkDefault = 'https://dubsism.files.wordpress.com/2017/12/image-not-found.png';
    console.log(rowUrl.length);

    return linkDefault;
  }
}
