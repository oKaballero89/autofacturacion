import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from './constant.service';
import { ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  public api_url;
  listenerSession = new EventEmitter<any>();

  constructor(public toast: ToastController, public constant: ConstantService, public router: Router, public loadingCtrl: LoadingController) { 
    this.api_url = this.constant.apiConnect;
  }

  async msgAlert(message){
    const toast = await this.toast.create({
      message: message,
      duration: 2000,
      position: 'top'
    });
    toast.present();
  }

  outSession(){
    localStorage.clear();
    this.router.navigate(['/']);
  }

  async loading(message) {
    const loading = await this.loadingCtrl.create({
      message: message,
      showBackdrop: true
    });
    return await loading.present();
    

  }


  // public function validateRfc($rfc) {
  //      return preg_match(“/^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$/“, $rfc);
  //  }
}
