import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantService {
  
  isprodCnk = false;
  baseUrl = window.location.protocol + '//' + window.location.host;
  host = window.location.host;
  public apiConnectDebu = 'http://localhost/proveedores-api/web/app_dev.php/api/v1/';
  public apiConnectProd ='http://gas-api.ilemuria.mx/web/app.php/api/v1/';

  public apiConnect = this.apiConnectDebu;
  
  conektaKey= (this.isprodCnk )? "--": "key_Nm78wRyBzzUy6Zssjf2guZg";
  companyApp = 'c1e0ea8a-87d4-11e9-9e4b-659de6a89800';
  constructor() { }
}