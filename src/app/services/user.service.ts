import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from './constant.service';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public api_url;
  private headers = {headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })};

  constructor(private http: HttpClient, public constant: ConstantService, private httpNative: HTTP) {
    this.api_url = this.constant.apiConnect;
  }

  createHeader(token) {
    this.headers = {headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'token': token })};
  }

  authUser(params){  
    return this.http.post(this.api_url+'user/login', params, this.headers);
  }

  restorePassword(email){
    let url = this.api_url+'user/restore_password';
    return this.http.post(url, { 'email' : email}, this.headers ) ;     
  }

  registerUser(params){
    this.headers = {headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded'})};
    let url = this.api_url+'user/register';
    return this.http.post(url, params, this.headers ) ; 
  }

  //user/{userId}/change-password
  changePassword(userId, params, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/change-password';
    return this.http.post(url, params, this.headers ) ; 
  }  

  userInfo(userId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId;
    return this.http.get(url, this.headers ) ; 
  }
  
  addCard(userId, params, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/addCard';
    return this.http.post(url, params, this.headers ) ; 
  }

  listCards(userId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/cards';
    return this.http.get(url, this.headers ) ; 
  }

  addRfc(params, userId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/addRfc';
    return this.http.post(url, params, this.headers ) ; 
  }

  listRfcs(userId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/Rfcs';
    return this.http.get(url, this.headers ) ; 
  }

  payments(userId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/tickets/payment';
    return this.http.get(url, this.headers ) ; 
  }

  deleteCard(userId, cardId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/card/'+cardId;
    return this.http.delete(url, this.headers ) ; 
  }

  deleteRFC(userId, rfcId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/rfc/'+rfcId;
    return this.http.delete(url, this.headers ) ; 
  }
}


