
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantService } from './constant.service';
import { HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class  TicketsService {

  public api_url;
  private headers = {headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })};

  constructor(private http: HttpClient, public constant: ConstantService, private httpNative: HTTP) {
    this.api_url = this.constant.apiConnect;
  }

  createHeader(token) {
    this.headers = {headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded', 'token': token })};
  }

  searchTicket( params, token){
    
    this.createHeader(token);
    let url = this.api_url+'tickets/buscar-code';
    return this.http.post(url, params, this.headers ) ; 
  }

  payTicket(params, userId, ticketId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/ticket/'+ticketId+'/pay';
    return this.http.post(url, params, this.headers ) ; 
  }

  paymentsTickets(userId,token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/tickets/payment';
    return this.http.get(url, this.headers ) ; 
  }

  getTicket(userId, ticketId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/ticket/'+ticketId;
    return this.http.get(url, this.headers ) ; 
  }

  sendToInvoice(userId, ticketId, params, token){
    
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/invoice/ticket/'+ticketId;
    return this.http.post(url, params, this.headers ) ;
  }

  getListInvoices(userId, token){
    
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/invoices';
    return this.http.get(url, this.headers ) ; 
  }

  searchTicketExpress( params, token){    
    this.createHeader('123');
    let url = this.api_url+'tickets/buscar-code';
    return this.http.post(url, params, this.headers ) ; 
  }

  sucursalesCompany(clientId ){
    this.createHeader('123');
    let url = this.api_url+'client/'+clientId+'/offices';
    return this.http.get(url, this.headers ) ; 
  }

  invoiceExpress(params, ticketId){
    this.createHeader('123');
    let url = this.api_url+'invoice-express/'+ticketId;    
    return this.http.post(url, params, this.headers ) ; 
  }

  promociones(userId, companyId, token){
    this.createHeader(token);
    let url = this.api_url+'user/'+userId+'/promotions/'+companyId+'/active-promo';    
    return this.http.get(url, this.headers ) ; 
  }



  requestPaymentCash(code, userId, token){
    this.createHeader('123');
    let url = this.api_url+'payment/'+code+'/'+userId;    
    return this.http.get(url,  this.headers ) ; 
  }
}