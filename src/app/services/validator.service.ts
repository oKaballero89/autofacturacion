import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor() { }

  validationLogin(inputs){
    console.log(inputs)
    if(inputs.username == '' || inputs.username == undefined ){
      return { success : false, msg: 'Debes ingresar usuario' };
    }

    if(inputs.password == '' || inputs.password == undefined ){
      return { success : false, msg: 'Debes ingresar contraseña' };
    }

    return { success : true, msg: 'todo bien' };

  }

  validationRegister(inputs){
    
    if(inputs.name == undefined || inputs.name == '' ){ return { success: false, msg: 'El nombre es requerido' } }
    if(inputs.last_name == undefined || inputs.last_name == '' ){ return { success: false, msg: 'Los apellidos son requeridos' } }
    if(inputs.email == undefined || inputs.email == '' ){ return { success: false, msg: 'El email es requerido' } }
    
    if( !this.validateEmail(inputs.email) ){return { success: false, msg: 'Debes ingresar un email válido' }   }
    if(inputs.password == undefined || inputs.password == '' ){ return { success: false, msg: 'La contraseña es requerida' } }

    if(inputs.cpassword == undefined || inputs.cpassword == '' ){ return { success: false, msg: 'Debes confirmar contraseña' } }

    if(inputs.password != inputs.cpassword ){ return { success: false, msg: 'Las contraseñas no coinciden' } }

    return { success: true, msg: '' }
  }

  validationInvoceExpress(inputs){
    if(inputs.email == undefined || inputs.email == '' ){ return { success: false, msg: 'El email es requerido' } }
    
    if( !this.validateEmail(inputs.email) ){return { success: false, msg: 'Debes ingresar un email válido' }   }

    if(inputs.razon_social == undefined || inputs.razon_social == '' ){ return { success: false, msg: 'Debes ingresar una razón social' } }

    if(inputs.RFC == undefined || inputs.RFC == '' ){ return { success: false, msg: 'Debes ingresar un RFC' } }
    
    return { success: true, msg: '' }
  }

  validateEmail(email){
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
}
