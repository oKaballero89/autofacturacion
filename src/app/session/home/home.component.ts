import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { InvoiceOutSesionComponent } from '../invoice-out-sesion/invoice-out-sesion.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  user:any = {};
  section='lost';

  constructor( public router: Router, 
    public nav: NavController,
    public modalCtrl: ModalController 
    ) { }

  ngOnInit() {}

  segmentChanged(ev: any) {
    if(ev.detail.value == 'login'){
      //this.nav.navigateForward('session/login');      
      this.openModal(LoginPage);
      setTimeout(() => {
        this.section = 'lost';
      }, 500);
      //this.router.navigate(['session/login']);  
    }else if(ev.detail.value == 'facturar'){
      setTimeout(() => {
        this.section = 'lost';
      }, 500);
      this.openModal(InvoiceOutSesionComponent);
      //this.router.navigate(['invoice']);  
    }

  }


  async openModal(component) {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: component,          
          componentProps: {}
    });
     
    modal.onDidDismiss().then((res:any) => {
      if(res.status == 'close'){
        this.section = '';
        // Pregunta si quiere facturar //
        
      }
    });
    
    await modal.present();
  }


}
