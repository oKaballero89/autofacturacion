import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { GeneralService } from 'src/app/services/general.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  email=null;
  constructor(public router: ActivatedRoute, public generalService: GeneralService, public userService: UserService) { }

  ngOnInit() {
  }

  restorePassword(){
    if(this.email == '' || this.email == undefined){
      this.generalService.msgAlert('Debes ingresar tu correo ');
      return;
    }

    this.userService.restorePassword(this.email).subscribe((res:any) => {
      console.log(':: enviar mail de restauració  n ::', res );
    } );

    this.userService.restorePassword(this.email).subscribe((res:any) =>{
     
      if(res.success ){     
        this.generalService.msgAlert('Tu petición se ha recibido, en breve recibiras un correo con tu nuevo acceso');
        this.email = null;
      }
      if(res.code == 401){
        this.generalService.msgAlert(res.msg);        
      }
    }, error =>{
      this.generalService.msgAlert(error.error.msg);   
    })


  }

}
