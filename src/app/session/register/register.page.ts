import { Component, OnInit } from '@angular/core';
import { ValidatorService } from 'src/app/services/validator.service';
import { UserService } from 'src/app/services/user.service';
import { GeneralService } from 'src/app/services/general.service';
import { ConstantService } from 'src/app/services/constant.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  register:any = {};
  
  constructor(public router: Router ,public validationService: ValidatorService, 
    public userService: UserService, public generalService: GeneralService, 
    public modalController: ModalController,
    public constant: ConstantService  ) { 
    console.log(this.constant.companyApp);
    this.register.company  = this.constant.companyApp;
  }

  ngOnInit() {
  }

  registerUser(){
    let validation = this.validationService.validationRegister(this.register);
    if(validation.success){
      this.userService.registerUser(this.register).subscribe((res:any)=>{
        if(res.code!=200){
          this.generalService.msgAlert(res.msg);
          return;
        }
        
        let msg = 'Usuario creado correctamente';
        this.generalService.msgAlert(msg);        
        this.register = {};

        setTimeout(()=>{
          let session = localStorage.setItem('concentGasApp', encodeURIComponent(JSON.stringify(res.msg)));
          setTimeout(() => {
            this.router.navigate(['tabs']);  
          }, 500);
        }, 2000)
      });
    }else{
      this.generalService.msgAlert(validation.msg);
      return;
    }  
  }

  close(){
    this.modalController.dismiss({'status': 'close'});
  }
}
