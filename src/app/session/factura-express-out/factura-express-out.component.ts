import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, LoadingController } from '@ionic/angular';
import { GeneralService } from 'src/app/services/general.service';
import { ValidatorService } from 'src/app/services/validator.service';
import { TicketsService } from 'src/app/services/tickets.service';
import { TicketDetailComponent } from 'src/app/invoices/ticket-detail/ticket-detail.component';
import { TicketDetailOutComponent } from '../ticket-detail-out/ticket-detail-out.component';

@Component({
  selector: 'app-factura-express-out',
  templateUrl: './factura-express-out.component.html',
  styleUrls: ['./factura-express-out.component.scss'],
})
export class FacturaExpressOutComponent implements OnInit {

  
  invoiceData:any = {};
  info;
  sucursal;

  constructor(
    public params: NavParams, 
    public modalCtrl: ModalController, 
    public generalService: GeneralService, 
    public validationService: ValidatorService, 
    public ticketsService: TicketsService,
    public loading: LoadingController
    ) { 
    this.info = this.params.get('msg');
    this.sucursal = this.params.get('sucursal');
  }

  ngOnInit() {}

  async showDetail(params) {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: TicketDetailOutComponent,          
          componentProps: {'msg': this.info}
    });
    
    await modal.present();
  }

  backTab(){
    this.modalCtrl.dismiss({'status': 'close'});    
  }

  sendInvoice(){
    this.invoiceData.RFC = this.invoiceData.RFC.toUpperCase().trim();

    this.generalService.loading('Generando tu factura');
    let validation = this.validationService.validationInvoceExpress(this.invoiceData);

    if(validation.success){     
      this.ticketsService.invoiceExpress(this.invoiceData, this.info.id).subscribe((res:any)=>{
        this.invoiceData = {};
        this.generalService.msgAlert('Tu factura ha sido creada correctamente, en breve recibiras un correo ');
        setTimeout(() => {
          this.loading.dismiss();
          this.modalCtrl.dismiss({status: 'update'});  
        }, 500);
        
      }, err=>{
        this.loading.dismiss();
        this.generalService.msgAlert('No se pudo realizar la facturación en este momento , intentalo más tarde.')
      });
    }else{
      this.generalService.msgAlert(validation.msg);
      return;
    }
  }

}
