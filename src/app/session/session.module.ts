import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SessionPage } from './session.page';
import { MatExpansionModule, MatIconModule, MatCardModule, MatButtonModule, MatListModule, MatProgressBarModule, MatMenuModule,MatRadioModule,MatToolbarModule , MatSelectModule, MatSidenavModule } from '@angular/material';

import {MatInputModule} from '@angular/material/input'
import { RegisterPage } from './register/register.page';
import { ForgotPasswordPage } from './forgot-password/forgot-password.page';
import { HomeComponent } from './home/home.component';
import { LoginPage } from './login/login.page';
import { InvoiceOutSesionComponent } from './invoice-out-sesion/invoice-out-sesion.component';
import { FacturaExpressOutComponent } from './factura-express-out/factura-express-out.component';
import { TicketDetailOutComponent } from './ticket-detail-out/ticket-detail-out.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'register',
    component: RegisterPage
  }, {
    path: 'forgot-password',
    component: ForgotPasswordPage
  }, {
    path: 'login',
    component: LoginPage
  },{
    path: 'invoice-Out',
    component: InvoiceOutSesionComponent
  } ,{
    path: 'invoice-express-Out',
    component: FacturaExpressOutComponent
  } ,{
    path: 'ticket-detail-Out',
    component: TicketDetailOutComponent
  }  
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,    
    FormsModule,
    MatInputModule,
    MatRadioModule,
    MatToolbarModule ,
    MatSelectModule,
    MatSidenavModule,
    MatExpansionModule,
    
    
    ],
  declarations: [SessionPage,
                RegisterPage,
                ForgotPasswordPage,
                HomeComponent,
                LoginPage,
                InvoiceOutSesionComponent,
                FacturaExpressOutComponent,
                TicketDetailOutComponent
              ]
})

export class SessionPageModule {}
