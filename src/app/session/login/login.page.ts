import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, NavController } from '@ionic/angular';
import { RegisterPage } from '../register/register.page';
import { Router } from '@angular/router';
import { ValidatorService } from 'src/app/services/validator.service';
import { UserService } from 'src/app/services/user.service';
import { GeneralService } from 'src/app/services/general.service';
import { ConstantService } from 'src/app/services/constant.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user:any={};
  constructor( public modalController: ModalController, public router: Router, public toast:  ToastController, 
    public validator: ValidatorService, public userService: UserService, 
    private generalServices: GeneralService, public navCtrl: NavController, 
    public constantService: ConstantService
    ) { 
      //this.user.username = 'ing.omarkaballero+3@gmail.com';
      this.user.username = 'ing.omarkaballero+3@gmail.com';
      this.user.password = "ee0bc188";
      this.user.companyId = this.constantService.companyApp;
      //this.user.password = "ee0bc188";
    }

  ngOnInit() {   
  }

  async openRegister(){
    const modal = await this.modalController.create({
      component: RegisterPage
    });

    return await modal.present();
  }

  async authSession(){
    
    const validation  = this.validator.validationLogin(this.user);
    localStorage.clear();
    if(validation.success){
      this.userService.authUser(this.user).subscribe((res:any) => {
        let session = localStorage.setItem('concentGasApp', encodeURIComponent(JSON.stringify(res.msg)));
        setTimeout(() => {
          this.generalServices.listenerSession.emit(true);
          this.router.navigate(['pago']);  
        }, 500);
        
      },error=>{
        let msg = error.error.msg;
        this.generalServices.msgAlert(msg);
      });
    }else{
      const toast = await this.toast.create({
        message: validation.msg,
        duration: 2000,
        position: 'top'
      });
      toast.present();
    } 
  }

  backTab(){
    this.modalController.dismiss({'status': 'close'});
    // this.navCtrl.pop();
    //this.router.navigate(['/']);  
  }


  
}
