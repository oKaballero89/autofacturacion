import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-ticket-detail-out',
  templateUrl: './ticket-detail-out.component.html',
  styleUrls: ['./ticket-detail-out.component.scss'],
})
export class TicketDetailOutComponent implements OnInit {

  info;
  rows:any = [];
  constructor(public params: NavParams, public modalCtrl: ModalController) {
    this.info = this.params.get('msg');    
    this.rows =  this.info.conceptos;
   }

  ngOnInit() {}

  backTab(){
    this.modalCtrl.dismiss();
  }

}
