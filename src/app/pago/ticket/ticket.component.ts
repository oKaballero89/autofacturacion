import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { TicketsService } from 'src/app/services/tickets.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
})
export class TicketComponent implements OnInit {

  conceptos:any =  [];
  subtotal;
  total;
  iva;
  userId;
  userToken;
  cards:any=[];
  company;
  user_token;
  cardPay;
  ticketId;
  typeView ;

  constructor(public params: NavParams, public userServices: UserService, public ticketService: TicketsService, public modalCtrl: ModalController) { 
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];
    this.company = b.company;
    this.user_token = b.session_token;
    
    const data = this.params.get('msg');
    console.log(data);
    const type = this.params.get('type');
    this.conceptos = data.conceptos;
    this.subtotal = data.subtotal;
    this.total = data.total;
    this.iva = data.iva;
    this.ticketId = data.id;
    
    if(type != 'pay'){
      setTimeout(() => {
        this.typeView = type;  
      }, 300);
    }

    //console.log('::: data', data);
    setTimeout(() => {
      this.getList();
    }, 200);
  }

  ngOnInit() {}

  PayTicket(){
    this.ticketService.payTicket({cardId:this.cardPay }, this.userId, this.ticketId, this.userToken).subscribe((res:any)=>{
      console.log(res);
      this.modalCtrl.dismiss()
    })
  }

  getList(){
    this.userServices.listCards(this.userId, this.userToken).subscribe((res:any)=>{
      this.cards = res.msg;
    }) ;
  }
  
  backTab(){
    this.modalCtrl.dismiss({});
  }

  requestPayment(){
    this.ticketService.requestPaymentCash(this.company, this.userId, this.userToken).subscribe((res:any) => {
      if(res.code == 200){
        this.modalCtrl.dismiss();
      }
    }, err => {

    })
  }

}
