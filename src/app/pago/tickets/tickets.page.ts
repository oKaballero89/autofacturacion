import { Component, OnInit } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Platform,  ModalController, LoadingController } from '@ionic/angular';
import { TicketsService } from 'src/app/services/tickets.service';
import { GeneralService } from 'src/app/services/general.service';
import { TicketComponent } from '../ticket/ticket.component';
import { Router } from '@angular/router';
import { TicketPayComponent } from '../ticket-pay/ticket-pay.component';
import { SendInvoiceComponent } from 'src/app/invoices/send-invoice/send-invoice.component';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.page.html',
  styleUrls: ['./tickets.page.scss'],
})
export class TicketsPage implements OnInit {

  isOn = false;
  scannedData: {};
  qrScan:any;
  inputTicket;
  user;
  user_token;
  company;
  codeInput;
  
  constructor( public platform: Platform, public ticketService: TicketsService, public generalService: GeneralService, 
    public modalCtrl: ModalController, public router: Router, public loading: LoadingController, public qrScanner: QRScanner ) { 
    this.user = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.company = this.user.company;
    this.user_token = this.user.session_token;

    //                                                         this.inputTicket = 'kjND9U';

    this.platform.backButton.subscribeWithPriority(0, () =>{
      document.getElementsByTagName("body")[0].style.opacity = "1";
      this.qrScan.unsubscribe();
    });
  }
  
  ngOnInit() {
    setTimeout(() => {
     // this.sendInvoice(this.inputTicket);  
    }, 1000);
    
  }

  activeSearch(code){
    this.codeInput = code;
    this.generalService.loading('Buscando ticket');
    let params = { 
      "companyId" : this.company.id,
      "noTicket": code
    }

    this.ticketService.searchTicket(params, this.user_token).subscribe((res:any)=>{
      this.loading.dismiss();

      if(res.code == 501){
        this.generalService.msgAlert(res.msg);
      }else if(res.code == 200){        
        setTimeout(()=>{
          this.openModal(res.msg);
        }, 400);
       
       
      }
    });
  }

  scann(){  
    // Optionally request the permission early
    this.qrScanner.prepare().then((status: QRScannerStatus) => {
      if(status.authorized){
        this.qrScanner.show();
        document.getElementsByTagName("body")[0].style.opacity = "0";        
        this.qrScan = this.qrScanner.scan().subscribe((textFound) => {
          document.getElementsByTagName("body")[0].style.opacity = "1";
          this.qrScan.unsubscribe();
          // console.info(textFound);
          // console.log('::: debio scanear :::');
          this.activeSearch(textFound);
          setTimeout(() => {
            
          }, 20);  

        }, err => {
          console.log(JSON.stringify(err));
        })
      }else if(status.denied){        
      }
    });          
  }

  async searchTicket(event: any){
    if(event.keyCode == 13){
      console.log('debe buscar ', this.inputTicket);
      this.inputTicket
      this.activeSearch(this.inputTicket);
    }
  }

  async presentModal(params) {
    console.log('debe abrir la pinshi modal ', params);
    const modal = await this.modalCtrl.create({
      component: TicketPayComponent,
      componentProps: {'msg': params}
    });
    
    return await modal.present();
  }

  async openModal(params) {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: TicketPayComponent,
          
          componentProps: {'msg': params}
    });
     
    modal.onDidDismiss().then((res) => {
      console.log(':: cerro modal', res);
      if(res.data.status == 'invoice'){
        // Pregunta si quiere facturar //
        this.sendInvoice(this.inputTicket);
      }
      else if(res.data.status == 'update'){
        this.backTab();
      }
    });
    
    await modal.present();
  }


  async sendInvoice(code) {
    const modal: HTMLIonModalElement =
       await this.modalCtrl.create({
          component: SendInvoiceComponent,          
          componentProps: {'ticket': this.codeInput}
    });
     
    modal.onDidDismiss().then((res:any) => {      
      if(res){
        // Pregunta si quiere facturar //      
        if(res.data.status == 'update'){          
          //this.router.navigate(['tabs/tab2']);  
          this.modalCtrl.dismiss({'status': 'update' });
        }
      }
    });
    
    await modal.present();
  }

  
  backTab(){
    //this.router.navigate(['tabs/tab2']);  
    this.modalCtrl.dismiss();
  }
}
