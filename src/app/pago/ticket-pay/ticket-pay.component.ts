import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { NavParams, ModalController, LoadingController } from '@ionic/angular';
import { TicketsService } from 'src/app/services/tickets.service';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-ticket-pay',
  templateUrl: './ticket-pay.component.html',
  styleUrls: ['./ticket-pay.component.scss'],
})
export class TicketPayComponent implements OnInit {

  conceptos:any =  [];
  subtotal;
  total;
  iva;
  userId;
  userToken;
  cards:any=[];
  company;
  user_token;
  cardPay;
  ticketId;
  typeView ;

  code;

  state='pay';

  constructor(public params: NavParams, public userServices: UserService, public ticketService: TicketsService, public modalCtrl: ModalController, 
    public generalService: GeneralService, public loading: LoadingController) { 
    const b = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.userId = b['id'];
    this.userToken = b['session_token'];
    this.company = b.company;
    this.user_token = b.session_token;
    
    const data = this.params.get('msg');
      this.code = data.code;
    console.log(data)
    const type = this.params.get('type');
    this.conceptos = data.conceptos;
    this.subtotal = data.subtotal;
    this.total = data.total;
    this.iva = data.iva;
    this.ticketId = data.id;
    
    if(type != 'pay'){
      setTimeout(() => {
        this.typeView = type;  
      }, 300);
    }
    setTimeout(() => {
      this.getList();
    }, 200);
  }

  ngOnInit() {}

  PayTicket(){

    if(!this.cardPay || this.cardPay == undefined || this.cardPay == ''  ){
      this.generalService.msgAlert('Debes seleccionr una tarjeta de pago');
      return;
    }

    this.generalService.loading('Procesando pago');
    this.ticketService.payTicket({cardId:this.cardPay }, this.userId, this.ticketId, this.userToken).subscribe((res:any)=>{
      this.loading.dismiss();
       
      if(res.code == 200){
        this.state = 'payed';
      }else{
        if(res.code == 501){
          this.generalService.msgAlert(res.msg);
        }else if(res.code == 500){
          this.generalService.msgAlert(res.message);
        }
      }
    }, err => {
      // errores que retorna coneckta caen aquí
      this.loading.dismiss();
      this.generalService.msgAlert(err.error.message);
    });

  }

  getList(){
    this.userServices.listCards(this.userId, this.userToken).subscribe((res:any)=>{
      this.cards = res.msg;
    }) ;
  }
  
  backTab(){
    this.modalCtrl.dismiss({});
  }

  invoice(param){
    if(param == 'si'){
      this.modalCtrl.dismiss({status: 'invoice'});
    }else{
      this.modalCtrl.dismiss({status: 'update'});
    }
  }

  requestPayment(){
    this.ticketService.requestPaymentCash(this.code, this.userId, this.userToken).subscribe((res:any) => {
      if(res.code == 200){
        this.modalCtrl.dismiss({status: 'update'});
      }
    }, err => {

    })
  }

}
