import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TicketsService } from '../services/tickets.service';
import { ModalController, LoadingController } from '@ionic/angular';
import { TicketComponent } from '../pago/ticket/ticket.component';
import { GeneralService } from '../services/general.service';
import { TicketsPage } from './tickets/tickets.page';
import { DetailComponent } from '../cfdi/detail/detail.component';
import { SendInvoiceComponent } from '../invoices/send-invoice/send-invoice.component';
@Component({
  selector: 'app-pago',
  templateUrl: './pago.page.html',
  styleUrls: ['./pago.page.scss'],
})
export class PagoPage implements OnInit {

  user;
  company;
  user_token;
  userId;
  rows:any = [];

  load=false;


  constructor(public router: Router,   public tickets: TicketsService, public modalCtrl: ModalController, public generalService: GeneralService,  public loading: LoadingController) {
    this.user = JSON.parse(decodeURIComponent(localStorage.getItem('concentGasApp')));
    this.company = this.user.company;
    this.userId = this.user.id;
    this.user_token = this.user.session_token;

  }

  ngOnInit(){
    
  }

  ionViewWillEnter(){
    setTimeout(() => {
        this.payments();
    }, 500);
  }

  // addPay(){
  //   this.router.navigate(['ticket/readTicket']);  
  // }

  payments(){
    this.generalService.loading('Cargando');
    this.tickets.paymentsTickets(this.userId, this.user_token).subscribe((res:any)=>{
      
      this.loading.dismiss();
      this.load = true;
      
      this.rows = res.msg ;
      console.log(':: ******** ::', res);
    }, err =>{
      
      this.loading.dismiss();
      if(err.status == 401){
        this.generalService.outSession();
      }
      
    
      //this.generalService.outSession();
    });
  }

  ticketDetail(ticketId){    
    this.tickets.getTicket(this.userId, ticketId, this.user_token).subscribe((res:any) => {
      console.log('debe entrar a modal');
      this.presentModal(res.msg);
    })
  }

  async presentModal(params) {
    const modal = await this.modalCtrl.create({
      component: TicketComponent,
      componentProps: {'msg': params, type: 'detail' }
    });
    
    return await modal.present();
  }

  async openInvoice(info) {
    let img = this. parsePdfXml(info);
    const modal = await this.modalCtrl.create({
      component: DetailComponent,
      componentProps: {'pathUrl': img}
    });
    
    return await modal.present();
  }


  async addPay() {
    const modal = await this.modalCtrl.create({
      component: TicketsPage,
      componentProps: { }
    });

    modal.onDidDismiss().then((res:any) => {      
      if(res){
        this.payments();
        // Pregunta si quiere facturar //      
        if(res.data){          
          //this.router.navigate(['tabs/tab2']);  
          //this.payments();
        }
      }
    });
    
    return await modal.present();
  }

  parsePdfXml(info){
    let PathPdf = info.pdf.split(".pdf");
    let img = PathPdf[0]+'.jpg';
    return img;
  }

  //SendInvoiceComponent
  async Facturar(code) {
    console.log(':: debe enviar ::');
    const modal = await this.modalCtrl.create({
      component: SendInvoiceComponent,
      componentProps: {'ticket': code }
    });

    modal.onDidDismiss().then((res:any) => {      
      if(res){
        // Pregunta si quiere facturar //      
        if(res.data){          
          //this.router.navigate(['tabs/tab2']);  
          this.payments();
        }
      }
    });
    return await modal.present();
  }

}
