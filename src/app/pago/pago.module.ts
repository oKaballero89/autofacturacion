import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagoPage } from './pago.page';
import { TicketsPage } from './tickets/tickets.page';
import { TicketComponent } from './ticket/ticket.component';
import { TicketPayComponent } from './ticket-pay/ticket-pay.component';
import { SendInvoiceComponent } from '../invoices/send-invoice/send-invoice.component';
import { MatExpansionModule, MatIconModule, MatCardModule, MatInputModule,MatButtonModule, MatListModule, MatProgressBarModule, MatMenuModule,MatRadioModule,MatToolbarModule , MatSelectModule, MatSidenavModule } from '@angular/material';
import { RfcComponent } from '../profile/rfc/rfc.component';
import { AddRfcComponent } from './add-rfc/add-rfc.component';
import { DetailComponent } from '../cfdi/detail/detail.component';

const routes: Routes = [
  {path: '',component: PagoPage},
  { path: 'readTicket', component: TicketsPage },
  { path: 'modalTicket-pay', component: TicketPayComponent},
  { path: 'modalInvoice-pay', component: SendInvoiceComponent},
  { path: 'modalRfc', component: AddRfcComponent},
  { path: 'detailTicket', component: TicketComponent},
  { path: 'detailInvoice', component: DetailComponent}

];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,        
    MatInputModule,
    MatRadioModule,
    MatToolbarModule ,
    MatSelectModule,
    MatSidenavModule,
    MatExpansionModule
  ],
  declarations: [PagoPage, 
    TicketsPage, 
    TicketPayComponent, 
    SendInvoiceComponent,
    AddRfcComponent,
    TicketComponent,
    DetailComponent
  ]
})
export class PagoPageModule {}
