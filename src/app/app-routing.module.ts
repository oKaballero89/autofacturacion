import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginRedirectService } from './services/login-redirect.service';


const routes: Routes = [
  { path: '', 
   loadChildren: './session/session.module#SessionPageModule',
   canActivate: [LoginRedirectService]
   },
  { path: 'sucursales', loadChildren: './sucursales/sucursales.module#SucursalesPageModule'},
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'ticket', loadChildren: './pago/pago.module#PagoPageModule'},
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'session', loadChildren: './session/session.module#SessionPageModule' },
  { path: 'promocion', loadChildren: './promotions/promotions.module#PromotionsPageModule' },
  // { path: 'login', loadChildren: './session/login/login.module#LoginPageModule' },
  { path: 'pago', loadChildren: './pago/pago.module#PagoPageModule' },  
  { path: 'invoice', loadChildren: './invoices/invoices.module#InvoicesPageModule' }

  
  //{ path: 'tickets', loadChildren: './pago/tickets/tickets.module#TicketsPageModule' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
